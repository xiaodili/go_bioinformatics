Stepic questions from Bioinformatics 2 that aren't on Rosalind yet

# Linux 
* Install with `sudo apt-get install golang`
* Set `GOPATH` with `export GOPATH=$HOME/dev/go`
* Repositories need to be put into `$GOPATH/src`

```

cd $GOPATH/src/bioinformatics

go install

$GOPATH/bin/bioinformatics decoding rosalind_ba10c.txt ba10c_output.txt

$GOPATH/bin/bioinformatics outcome_likelihood rosalind_ba10d.txt ba10d_output.txt

$GOPATH/bin/bioinformatics small_parsimony_unrooted rosalind_ba7g.txt ba7g_output.txt

```

# Caveats

* Make sure file parsing is working correctly by checking for spaces vs. tabs
* In `outcome_likelihood`, watch out for precision errors (give it to 11 dp, as per the extra dataset)
