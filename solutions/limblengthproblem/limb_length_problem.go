package limblengthproblem

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type rosalindInput struct {
	n              int
	j              int
	distanceMatrix [][]int
}

func getInputFromFile(absfilename string) rosalindInput {

	//opening file
	inputFile, err := os.Open(absfilename)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	//n and j
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())
	scanner.Scan()
	j, _ := strconv.Atoi(scanner.Text())

	//distance matrix
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}

	for i := 0; i < n; i++ {
		scanner.Scan()
		line := scanner.Text()
		elements := strings.Split(line, " ")
		for j := 0; j < n; j++ {
			distance, _ := strconv.Atoi(elements[j])
			distanceMatrix[i][j] = distance
		}
	}

	return rosalindInput{n, j, distanceMatrix}
}

func getInputFromSample() rosalindInput {
	n := 4
	j := 1
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}
	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 21
	distanceMatrix[0][3] = 22

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 12
	distanceMatrix[1][3] = 13

	distanceMatrix[2][0] = 21
	distanceMatrix[2][1] = 12
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 13

	distanceMatrix[3][0] = 22
	distanceMatrix[3][1] = 13
	distanceMatrix[3][2] = 13
	distanceMatrix[3][3] = 0

	return rosalindInput{n, j, distanceMatrix}
}

func solve(rosalindInput rosalindInput) int {
	n := rosalindInput.n
	j := rosalindInput.j
	distanceMatrix := rosalindInput.distanceMatrix

	minimumDistance := math.MaxInt16
	for i := 0; i < n; i++ {
		for k := 0; k < n; k++ {
			if i != j && k != j {
				candidate := (distanceMatrix[i][j] + distanceMatrix[j][k] - distanceMatrix[i][k]) / 2
				if candidate < minimumDistance {
					minimumDistance = candidate
				}
			}
		}
	}
	return minimumDistance
}

func outputToConsole(minimumDistance int) {
	fmt.Println(minimumDistance)
}

func outputToFile(path string, minimumDistance int) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	f.WriteString(strconv.Itoa(minimumDistance))
	f.Sync()
}

type solver struct {
}

//exposed
func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	limbLength := solve(input)
	outputToConsole(limbLength)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	limbLength := solve(input)
	outputToFile(outputPath, limbLength)
}

var Solver = solver{}
