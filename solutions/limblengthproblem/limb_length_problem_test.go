package limblengthproblem

import (
	"fmt"
	"path/filepath"
	"testing"
)

func TestSampleSolution(t *testing.T) {
	//SolveSample()
	// Output: 2
}

func TestForQuiz(t *testing.T) {
	n := 4
	j := 2
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}
	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 14
	distanceMatrix[0][2] = 17
	distanceMatrix[0][3] = 17

	distanceMatrix[1][0] = 14
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 7
	distanceMatrix[1][3] = 13

	distanceMatrix[2][0] = 17
	distanceMatrix[2][1] = 7
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 16

	distanceMatrix[3][0] = 17
	distanceMatrix[3][1] = 13
	distanceMatrix[3][2] = 16
	distanceMatrix[3][3] = 0

	ri := rosalindInput{
		distanceMatrix: distanceMatrix,
		n:              n,
		j:              j,
	}
	result := solve(ri)
	fmt.Printf("result: %d\n", result)
}

func TestParsing(t *testing.T) {
	path, _ := filepath.Abs("../../assets/limblengthproblemsample.txt")
	result := getInputFromFile(path)
	if !(result.n == 4 && result.j == 1) {
		t.Errorf("parsing has gone wrong: n: %d, j: %d", result.n, result.j)
	}
	if !(result.distanceMatrix[2][0] == 21 && result.distanceMatrix[3][0] == 22) {
		t.Errorf("parsing has gone wrong: [2][0]: %d, [3][0]: %d", result.distanceMatrix[2][0], result.distanceMatrix[3][0])
	}
}
