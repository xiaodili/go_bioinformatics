package smallparsimonyproblem

import (
	//"fmt"
	"path/filepath"
	"testing"
)

func TestGetInputFromSample(t *testing.T) {
	rosalindInput := getInputFromSample()
	expectedN := 4
	actualN := rosalindInput.numLeaves
	if actualN != expectedN {
		t.Errorf("actualN: %d, expectedN: %d", actualN, expectedN)
	}
}

func TestGetInputFromFile(t *testing.T) {
	//need to run this in this files directory
	path, _ := filepath.Abs("../../assets/rosalind_sample.txt")
	rosalindInput := getInputFromFile(path)
	expectedN := 4
	actualN := rosalindInput.numLeaves
	if actualN != expectedN {
		t.Errorf("actualN: %d, expectedN: %d", actualN, expectedN)
	}
	actualLen := len(rosalindInput.adjacencyList)
	expectedLen := 6
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}
}

func TestFormatOutput(t *testing.T) {
	parsimonyScore := 16
	nodes := make([]*node, 2, 2)
	nodes[0] = &node{0, "TCGA", make(map[int]weightedEdge)}
	nodes[1] = &node{1, "GACT", make(map[int]weightedEdge)}
	nodes[0].neighbours[1] = weightedEdge{2, nodes[1]}
	nodes[1].neighbours[0] = weightedEdge{2, nodes[0]}
	rosalindOutput := rosalindOutput{parsimonyScore, nodes}
	formattedOutput := formatOutput(rosalindOutput)
	expectedStr0 := "16"
	actualStr0 := formattedOutput[0]
	if actualStr0 != expectedStr0 {
		t.Errorf("actualStr0: %s, expectedStr0: %s", actualStr0, expectedStr0)
	}
	expectedLen := 3
	actualLen := len(formattedOutput)
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}
	expectedStr1 := "TCGA->GACT:2"
	actualStr1 := formattedOutput[1]
	if actualStr1 != expectedStr1 {
		t.Errorf("actualStr1: %s, expectedStr1: %s", actualStr1, expectedStr1)
	}
}

func TestCalculateSBaseCase(t *testing.T) {
	nodes := make([]*parsimonyNode, 3, 3)
	nodes[0] = &(parsimonyNode{0, false, 'C', make(map[byte]int), minScore{'C', 0}, nil, nil})
	nodes[1] = &(parsimonyNode{1, false, 'C', make(map[byte]int), minScore{'C', 0}, nil, nil})
	nodes[2] = &(parsimonyNode{2, false, 0, make(map[byte]int), minScore{}, nodes[0], nodes[1]})

	alphabet := []byte{'A', 'C', 'T', 'G'}
	expectedScoreForRootT := 2
	root := calculateS(nodes, alphabet)
	actualScoreForRootT := root.s['T']
	if actualScoreForRootT != expectedScoreForRootT {
		t.Errorf("actualScoreForRootT: %d, expectedScoreForRootT: %d", actualScoreForRootT, expectedScoreForRootT)
	}
}

func TestCalculateS(t *testing.T) {
	nodes := GetExampleCharacterTree()
	alphabet := []byte{'A', 'C', 'T', 'G'}
	expectedScoreRootC := 3
	actualScoreRootC := calculateS(nodes, alphabet).s['C']
	if actualScoreRootC != expectedScoreRootC {
		t.Errorf("actualScoreRootC: %d, expectedScoreRootC: %d", actualScoreRootC, expectedScoreRootC)
	}
}

func TestBacktrackTree(t *testing.T) {
	nodes := GetExampleCharacterTree()
	alphabet := []byte{'A', 'C', 'T', 'G'}
	root := calculateS(nodes, alphabet)
	BacktrackTree(root)

	actualParsimony := root.min.score
	expectedParsimony := 3
	if actualParsimony != expectedParsimony {
		t.Errorf("actualParsimony: %d, expectedParsimony: %d", actualParsimony, expectedParsimony)
	}
}

func TestCreateCharacterTree(t *testing.T) {
	rosalindInput := getInputFromSample()

	parsimonyTree := createCharacterTree(&rosalindInput, 0)
	actualChar0 := parsimonyTree[0].char
	var expectedChar0 byte
	expectedChar0 = 'C'
	if actualChar0 != expectedChar0 {
		t.Errorf("actualChar0: %d, expectedChar0: %d", actualChar0, expectedChar0)
	}

	parsimonyTree = createCharacterTree(&rosalindInput, 1)
	actualChar1 := parsimonyTree[0].char
	var expectedChar1 byte
	expectedChar1 = 'A'
	if actualChar1 != expectedChar1 {
		t.Errorf("actualChar1: %d, expectedChar1: %d", actualChar1, expectedChar1)
	}
}

//helpers

//as per https://stepic.org/lesson/The-Small-Parsimony-Problem-10335/step/8?course=Bioinformatics-Algorithms-(Part-2)&unit=2076
func GetExampleCharacterTree() []*parsimonyNode {
	nodes := make([]*parsimonyNode, 15, 15)
	nodes[0] = &(parsimonyNode{0, false, 'C', make(map[byte]int), minScore{'C', 0}, nil, nil})
	nodes[1] = &(parsimonyNode{1, false, 'C', make(map[byte]int), minScore{'C', 0}, nil, nil})
	nodes[2] = &(parsimonyNode{2, false, 'A', make(map[byte]int), minScore{'A', 0}, nil, nil})
	nodes[3] = &(parsimonyNode{3, false, 'C', make(map[byte]int), minScore{'C', 0}, nil, nil})
	nodes[4] = &(parsimonyNode{4, false, 'G', make(map[byte]int), minScore{'G', 0}, nil, nil})
	nodes[5] = &(parsimonyNode{5, false, 'G', make(map[byte]int), minScore{'G', 0}, nil, nil})
	nodes[6] = &(parsimonyNode{6, false, 'T', make(map[byte]int), minScore{'T', 0}, nil, nil})
	nodes[7] = &(parsimonyNode{7, false, 'C', make(map[byte]int), minScore{'C', 0}, nil, nil})

	nodes[8] = &(parsimonyNode{8, false, 0, make(map[byte]int), minScore{}, nodes[0], nodes[1]})
	nodes[9] = &(parsimonyNode{9, false, 0, make(map[byte]int), minScore{}, nodes[2], nodes[3]})
	nodes[10] = &(parsimonyNode{10, false, 0, make(map[byte]int), minScore{}, nodes[4], nodes[5]})
	nodes[11] = &(parsimonyNode{11, false, 0, make(map[byte]int), minScore{}, nodes[6], nodes[7]})

	nodes[12] = &(parsimonyNode{12, false, 0, make(map[byte]int), minScore{}, nodes[8], nodes[9]})
	nodes[13] = &(parsimonyNode{13, false, 0, make(map[byte]int), minScore{}, nodes[10], nodes[11]})

	nodes[14] = &(parsimonyNode{13, false, 0, make(map[byte]int), minScore{}, nodes[12], nodes[13]})

	return nodes
}
