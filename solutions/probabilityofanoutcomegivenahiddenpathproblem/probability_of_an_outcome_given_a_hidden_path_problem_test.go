package probabilityofanoutcomegivenahiddenpathproblem

import (
	"testing"
)

func TestGetFromFile(t *testing.T) {
	path := "../../assets/rosalind_sample.txt"
	ri := getInputFromFile(path)

	actualHiddenPath := ri.hiddenPath
	expectedHiddenPath := "BAAAAAAAAA"
	if actualHiddenPath != expectedHiddenPath {
		t.Errorf("actualHiddenPath: %s, expectedHiddenPath: %s", actualHiddenPath, expectedHiddenPath)
	}

	actualEmittedString := ri.emittedString
	expectedEmittedString := "zzzyxyyzzx"
	if actualEmittedString != expectedEmittedString {
		t.Errorf("actualEmittedString: %s, expectedEmittedString: %s", actualEmittedString, expectedEmittedString)
	}

	emission := ri.emission
	actualLastElement := emission[1][2]
	expectedLastElement := float32(0.203)

	if actualLastElement != expectedLastElement {
		t.Errorf("actualLastElement: %f, expectedLastElement: %f", actualLastElement, expectedLastElement)
	}

}
