package probabilityofanoutcomegivenahiddenpathproblem

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 11, 64)
	fmt.Println(probabilityStr)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 11, 64)
	outputToFile([]string{probabilityStr}, outputPath)
}

var Solver = solver{}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func solve(ri rosalindInput) float64 {
	emittedString := ri.emittedString
	alphabet := ri.alphabet
	hiddenPath := ri.hiddenPath
	states := ri.states
	emission := ri.emission

	letterToEmissionIndex := make(map[byte]int)
	for index, value := range alphabet {
		letterToEmissionIndex[value] = index
	}
	stateToEmissionIndex := make(map[byte]int)
	for index, value := range states {
		stateToEmissionIndex[value] = index
	}

	currentProbability := float64(1)
	for i, letter := range []byte(emittedString) {
		state := hiddenPath[i]
		currentProbability *= float64(
			emission[stateToEmissionIndex[state]][letterToEmissionIndex[letter]])
	}
	return currentProbability
}

func getInputFromSample() rosalindInput {
	emittedString := "zzzyxyyzzx"
	alphabet := []byte{'x', 'y', 'z'}
	states := []byte{'A', 'B'}
	hiddenPath := "BAAAAAAAAA"
	emission := make([][]float32, 2)
	emission[0] = []float32{0.176, 0.596, 0.228}
	emission[1] = []float32{0.225, 0.572, 0.203}
	return rosalindInput{emittedString, alphabet, hiddenPath, states, emission}
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	emittedString := scanner.Text()

	scanner.Scan() //-------

	scanner.Scan()
	alphabetStr := scanner.Text()
	alphabetStrs := strings.Split(alphabetStr, " ")
	alphabet := make([]byte, len(alphabetStrs), len(alphabetStrs))
	for i, value := range alphabetStrs {
		alphabet[i] = value[0]
	}

	scanner.Scan() //-------

	scanner.Scan()
	hiddenPath := scanner.Text()

	scanner.Scan() //-------

	scanner.Scan()
	statesStr := scanner.Text()
	stateStrs := strings.Split(statesStr, " ")
	states := make([]byte, len(stateStrs), len(stateStrs))
	for i, value := range stateStrs {
		states[i] = value[0]
	}

	scanner.Scan() //-------

	scanner.Scan() // x y z

	emissions := make([][]float32, len(states), len(states))
	rowNum := 0
	for scanner.Scan() {
		emissionRowStr := strings.Split(scanner.Text(), "\t")[1:]
		emissions[rowNum] = make([]float32, len(alphabet))
		for i := 0; i < len(alphabet); i++ {
			emissionElem, _ := strconv.ParseFloat(emissionRowStr[i], 32)
			emissions[rowNum][i] = float32(emissionElem)
		}
		rowNum += 1
	}
	return rosalindInput{emittedString, alphabet, hiddenPath, states, emissions}
}

type rosalindInput struct {
	emittedString string
	alphabet      []byte
	hiddenPath    string
	states        []byte
	emission      [][]float32
}

//helpers
func check(e error) {
	if e != nil {
		panic(e)
	}
}
