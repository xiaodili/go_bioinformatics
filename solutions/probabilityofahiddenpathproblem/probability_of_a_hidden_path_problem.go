package probabilityofahiddenpathproblem

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

var Solver = solver{}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 11, 64)
	fmt.Println(probabilityStr)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 11, 64)
	outputToFile([]string{probabilityStr}, outputPath)
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func solve(ri rosalindInput) float64 {
	hiddenPath := ri.hiddenPath
	states := ri.states
	emission := ri.emission
	stateToEmissionIndex := make(map[byte]int)
	for index, value := range states {
		stateToEmissionIndex[value] = index
	}
	currentProbability := float64(1)
	for i := 1; i < len(hiddenPath); i++ {
		fromState := hiddenPath[i-1]
		toState := hiddenPath[i]
		currentProbability *= float64(emission[stateToEmissionIndex[fromState]][stateToEmissionIndex[toState]])
	}
	return currentProbability * float64(0.5)
}

func getInputFromSample() rosalindInput {
	hiddenPath := "ABABBBAAAA"
	states := []byte{'A', 'B'}
	emission := make([][]float32, 2)
	emission[0] = []float32{0.377, 0.623}
	emission[1] = []float32{0.26, 0.74}
	return rosalindInput{hiddenPath, states, emission}
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	hiddenPath := scanner.Text()

	scanner.Scan() //-------

	scanner.Scan()
	statesStr := scanner.Text()
	stateStrs := strings.Split(statesStr, " ")
	states := make([]byte, len(stateStrs), len(stateStrs))
	for i, value := range stateStrs {
		states[i] = value[0]
	}

	scanner.Scan() //-------
	scanner.Scan() // A B

	emissions := make([][]float32, len(states), len(states))
	rowNum := 0
	for scanner.Scan() {
		emissionRowStr := strings.Split(scanner.Text(), "\t")[1:]
		emissions[rowNum] = make([]float32, len(states))
		for i := 0; i < len(states); i++ {
			emissionElem, _ := strconv.ParseFloat(emissionRowStr[i], 32)
			emissions[rowNum][i] = float32(emissionElem)
		}
		rowNum += 1
	}
	return rosalindInput{hiddenPath, states, emissions}
}

type rosalindInput struct {
	hiddenPath string
	states     []byte
	emission   [][]float32
}

//helpers
func check(e error) {
	if e != nil {
		panic(e)
	}
}
