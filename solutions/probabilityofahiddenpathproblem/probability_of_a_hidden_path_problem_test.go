package probabilityofahiddenpathproblem

import (
	"testing"
)

func TestSolveFromSample(t *testing.T) {
	ri := getInputFromSample()
	actualProbability := solve(ri)
	expectedProbability := 0.000384928691755
	if actualProbability != expectedProbability {
		t.Errorf("actualProbability: %f, expectedProbability: %f", actualProbability, expectedProbability)
	}
}

func TestGetFromFile(t *testing.T) {
	path := "../../assets/rosalind_sample.txt"
	ri := getInputFromFile(path)
	actualHiddenPath := ri.hiddenPath
	expectedHiddenPath := "ABABBBAAAA"
	if actualHiddenPath != expectedHiddenPath {
		t.Errorf("actualHiddenPath: %s, expectedHiddenPath: %s", actualHiddenPath, expectedHiddenPath)
	}
}
