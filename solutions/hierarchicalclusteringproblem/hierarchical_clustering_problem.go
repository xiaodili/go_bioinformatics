package hierarchicalclusteringproblem

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

var Solver = solver{}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	printouts := solve(input)
	result := formatOutput(printouts)
	outputToConsole(result)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	printouts := solve(input)
	result := formatOutput(printouts)
	outputToFile(result, outputPath)
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())

	distanceMatrix := make([][]float32, n)
	var rowIndex = 0
	for scanner.Scan() {
		distanceMatrix[rowIndex] = parseDistanceRow(scanner.Text(), n)
		rowIndex += 1
	}
	return rosalindInput{distanceMatrix: distanceMatrix, n: n}
}

func parseDistanceRow(rowStr string, n int) []float32 {
	row := make([]float32, n)
	rowArr := strings.Split(rowStr, " ")
	for i, value := range rowArr {
		distance, _ := strconv.ParseFloat(value, 32)
		row[i] = float32(distance)
	}
	return row
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func formatOutput(printouts [][]int) []string {
	clusterStrings := make([]string, len(printouts))
	for i := range printouts {
		clusterIds := printouts[i]
		for _, id := range clusterIds {
			clusterStrings[i] += strconv.Itoa(id) + " "
		}
		clusterStrings[i] = clusterStrings[i][:len(clusterStrings[i])-1]
	}
	return clusterStrings
}

func outputToConsole(output []string) {
	for i := range output {
		fmt.Println(output[i])
	}
}

type node struct {
	id int
}

type cluster struct {
	nodes []*node
}

func solve(ri rosalindInput) [][]int {
	return hierarchicalClustering(ri.distanceMatrix, ri.n)
}

func hierarchicalClustering(D [][]float32, n int) [][]int {

	// initialising nodes and clusters
	var printouts [][]int
	nodes := initNodes(n)
	clusters := initClusters(nodes, n)
	di2Ci := initDi2Ci(n)
	nextAvailableClusterId := n

	// main loop
	for len(clusters) > 1 {
		c1, c2 := findClosestClusters(D)
		newCluster := createCluster(clusters, di2Ci[c1], di2Ci[c2])
		newDistances := calculateNewDistances(D, c1, c2, len(clusters[di2Ci[c1]].nodes), len(clusters[di2Ci[c2]].nodes))
		D = removeRowsAndColumns(D, c1, c2)
		delete(clusters, di2Ci[c1])
		delete(clusters, di2Ci[c2])
		di2Ci = updateDi2Ci(di2Ci, c1, c2, nextAvailableClusterId)
		clusters[nextAvailableClusterId] = newCluster
		D = appendNewDistances(D, newDistances)
		nextAvailableClusterId += 1
		printouts = append(printouts, getIdsOneIndexed(newCluster.nodes))
	}
	return printouts
}

func calculateNewDistances(D [][]float32, c1, c2, c1Len, c2Len int) []float32 {
	n := len(D)
	newDistances := make([]float32, n-1, n-1)
	currentIndex := 0
	for i := range D {
		if i != c1 && i != c2 {
			newDistances[currentIndex] = (D[i][c1]*float32(c1Len) + D[i][c2]*float32(c2Len)) / float32(c1Len+c2Len)
			currentIndex += 1
		}
	}
	newDistances[currentIndex] = 0
	return newDistances
}

func getIdsOneIndexed(nodes []*node) []int {
	ids := make([]int, len(nodes))
	for i := range nodes {
		ids[i] = nodes[i].id + 1
	}
	return ids
}

func appendNewDistances(D [][]float32, newDistances []float32) [][]float32 {
	for i := range D {
		D[i] = append(D[i], newDistances[i])
	}
	return append(D, newDistances)
}

func updateDi2Ci(di2ni []int, c1, c2, newIndex int) []int {
	di2ni = append(di2ni[0:c2], di2ni[c2+1:len(di2ni)]...)
	di2ni = append(di2ni[0:c1], di2ni[c1+1:len(di2ni)]...)
	return append(di2ni, newIndex)
}

func copy2DArray(D [][]float32, n int) [][]float32 {
	deepCopy := make([][]float32, n)
	for i := range D {
		deepCopy[i] = make([]float32, n)
		copy(deepCopy[i], D[i])
	}
	return deepCopy
}
func createCluster(clusters map[int]*cluster, c1, c2 int) *cluster {
	nodes1 := clusters[c1].nodes
	nodes2 := clusters[c2].nodes
	return &cluster{nodes: append(nodes1, nodes2...)}
}

func initNodes(n int) []*node {
	nodes := make([]*node, n, n*2)
	for i := 0; i < n; i++ {
		nodes[i] = &node{id: i}
	}
	return nodes
}

func initClusters(nodes []*node, n int) map[int]*cluster {
	clusters := make(map[int]*cluster, n)
	for i := 0; i < n; i++ {
		clusters[i] = &cluster{nodes: []*node{nodes[i]}}
	}
	return clusters
}

func initDi2Ci(n int) []int {
	di2Ni := make([]int, n, n)
	for i := 0; i < n; i++ {
		di2Ni[i] = i
	}
	return di2Ni
}

func removeRowsAndColumns(D [][]float32, i, j int) [][]float32 {
	D = slice(D, j)
	return slice(D, i)
}

func slice(D [][]float32, index int) [][]float32 {
	n := len(D)
	for i := range D {
		D[i] = append(D[i][0:index], D[i][index+1:n]...)
	}
	return append(D[0:index], D[index+1:n]...)
}

func findClosestClusters(D [][]float32) (int, int) {
	n := len(D)
	minimumDistance := float32(math.MaxFloat32)
	var c1, c2 int
	//iteration on upper triangle
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			if D[i][j] < minimumDistance {
				minimumDistance = D[i][j]
				c1 = i
				c2 = j
			}
		}
	}
	return c1, c2
}

func getInputFromSample() rosalindInput {
	n := 7
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	distanceMatrix[0][0] = 0.00
	distanceMatrix[0][1] = 0.74
	distanceMatrix[0][2] = 0.85
	distanceMatrix[0][3] = 0.54
	distanceMatrix[0][4] = 0.83
	distanceMatrix[0][5] = 0.92
	distanceMatrix[0][6] = 0.89

	distanceMatrix[1][0] = 0.74
	distanceMatrix[1][1] = 0.00
	distanceMatrix[1][2] = 1.59
	distanceMatrix[1][3] = 1.35
	distanceMatrix[1][4] = 1.20
	distanceMatrix[1][5] = 1.48
	distanceMatrix[1][6] = 1.55

	distanceMatrix[2][0] = 0.85
	distanceMatrix[2][1] = 1.59
	distanceMatrix[2][2] = 0.00
	distanceMatrix[2][3] = 0.63
	distanceMatrix[2][4] = 1.13
	distanceMatrix[2][5] = 0.69
	distanceMatrix[2][6] = 0.73

	distanceMatrix[3][0] = 0.54
	distanceMatrix[3][1] = 1.35
	distanceMatrix[3][2] = 0.63
	distanceMatrix[3][3] = 0.00
	distanceMatrix[3][4] = 0.66
	distanceMatrix[3][5] = 0.43
	distanceMatrix[3][6] = 0.88

	distanceMatrix[4][0] = 0.83
	distanceMatrix[4][1] = 1.20
	distanceMatrix[4][2] = 1.13
	distanceMatrix[4][3] = 0.66
	distanceMatrix[4][4] = 0.00
	distanceMatrix[4][5] = 0.72
	distanceMatrix[4][6] = 0.55

	distanceMatrix[5][0] = 0.92
	distanceMatrix[5][1] = 1.48
	distanceMatrix[5][2] = 0.69
	distanceMatrix[5][3] = 0.43
	distanceMatrix[5][4] = 0.72
	distanceMatrix[5][5] = 0.00
	distanceMatrix[5][6] = 0.80

	distanceMatrix[6][0] = 0.89
	distanceMatrix[6][1] = 1.55
	distanceMatrix[6][2] = 0.73
	distanceMatrix[6][3] = 0.88
	distanceMatrix[6][4] = 0.55
	distanceMatrix[6][5] = 0.80
	distanceMatrix[6][6] = 0.00

	return rosalindInput{n, distanceMatrix}
}

type rosalindInput struct {
	n              int
	distanceMatrix [][]float32
}
