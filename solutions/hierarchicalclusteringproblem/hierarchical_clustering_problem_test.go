package hierarchicalclusteringproblem

import (
	"fmt"
	"testing"
)

func TestHierarchicalClustering(t *testing.T) {
	ri := getInputFromSample()
	D := ri.distanceMatrix
	n := ri.n
	printouts := hierarchicalClustering(D, n)
	for i := range printouts {
		fmt.Printf("printouts[i]: %d\n", printouts[i])
	}
}

func TestCreateCluster(t *testing.T) {
	n := 7
	nodes := initNodes(n)
	clusters := initClusters(nodes, n)
	c1 := 3
	c2 := 5
	newCluster := createCluster(clusters, c1, c2)
	expectedLen := 2
	actualLen := len(newCluster.nodes)
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}
}

func TestAverageDistance(t *testing.T) {
	ri := getInputFromSample()
	nodes := initNodes(ri.n)
	c1 := cluster{nodes: []*node{nodes[2], nodes[3]}}
	c2 := cluster{nodes: []*node{nodes[1]}}
	avgD := averageDistance(ri.distanceMatrix, c1, c2)
	fmt.Printf("avgD: %f\n", avgD)
}

func TestFormatOutput(t *testing.T) {
	printouts := make([][]int, 3)
	printouts[0] = []int{1, 2, 3, 4}
	printouts[1] = []int{1, 3, 3, 4}
	printouts[2] = []int{2, 2, 3, 4}
	result := formatOutput(printouts)
	expectedFstString := "1 2 3 4"
	actualFstString := result[0]
	if actualFstString != expectedFstString {
		t.Errorf("actualFstString: %d, expectedFstString: %d", actualFstString, expectedFstString)
	}
}
