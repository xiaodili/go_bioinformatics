package smallparsimonyunrootedproblem

import (
	"testing"
)

func TestGetInputFromSample(t *testing.T) {
	input := getInputFromSample()
	expectedNumLeaves := 4
	actualNumLeaves := input.numLeaves
	if actualNumLeaves != expectedNumLeaves {
		t.Errorf("actualNumLeaves: %d, expectedNumLeaves: %d", actualNumLeaves, expectedNumLeaves)
	}
	expectedNumEdges := 10
	actualNumEdges := len(input.adjacencyList)
	if actualNumEdges != expectedNumEdges {
		t.Errorf("actualNumEdges: %d, expectedNumEdges: %d", actualNumEdges, expectedNumEdges)
	}
}

func TestConvertToRootedInput(t *testing.T) {
	input := getInputFromSample()
	input = convertToRootedInput(input)
	expectedLen := 6
	actualLen := len(input.adjacencyList)
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}

	expectedNumLeaves := 4
	actualNumLeaves := input.numLeaves
	if actualNumLeaves != expectedNumLeaves {
		t.Errorf("actualNumLeaves: %d, expectedNumLeaves: %d", actualNumLeaves, expectedNumLeaves)
	}
}
