package smallparsimonyunrootedproblem

import (
	"bufio"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()

	input = convertToRootedInput(input)
	rosalindOutput := solve(input)
	formattedOutput := formatOutput(rosalindOutput)
	outputToConsole(formattedOutput)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)

	input = convertToRootedInput(input)

	rosalindOutput := solve(input)
	formattedOutput := formatOutput(rosalindOutput)
	outputToFile(formattedOutput, outputPath)
}

var Solver = solver{}

//functions

func convertToRootedInput(ri rosalindInput) rosalindInput {
	n := ri.numLeaves
	adjacencyList := ri.adjacencyList

	rootIndex := 2*n - 2
	adjListLen := len(adjacencyList)
	rootedAdjListLen := adjListLen/2 + 1

	//seems like we can skip all the odd ones, then add in the root edges
	rootedAdjacencyList := make([]rosalindInputEdge, rootedAdjListLen, rootedAdjListLen)
	for i := 0; i < rootedAdjListLen-1; i++ {
		rootedAdjacencyList[i] = adjacencyList[2*i+1]
	}
	unrootedNodes := adjacencyList[adjListLen-1]
	rootedAdjacencyList[rootedAdjListLen-2] = rosalindInputEdge{source: strconv.Itoa(rootIndex), destination: unrootedNodes.source}
	rootedAdjacencyList[rootedAdjListLen-1] = rosalindInputEdge{source: strconv.Itoa(rootIndex), destination: unrootedNodes.destination}
	return rosalindInput{n, rootedAdjacencyList}
}

//wow... this needs to work on each of these character of string
func solve(rosalindInput rosalindInput) rosalindOutput {
	//I *think* this is a reliable way of getting the overall DNA string length
	adjacencyList := rosalindInput.adjacencyList
	n := rosalindInput.numLeaves
	dnaLen := len(adjacencyList[0].destination)

	parsimonyTrees := make([][]*parsimonyNode, dnaLen, dnaLen)
	alphabet := []byte{'A', 'C', 'T', 'G'}
	parsimonyScore := 0

	for i := 0; i < dnaLen; i++ {
		parsimonyTrees[i] = createCharacterTree(&rosalindInput, i)
		root := calculateS(parsimonyTrees[i], alphabet)
		BacktrackTree(root)
		parsimonyScore += root.min.score
	}

	nodes := make([]*node, 2*n-1, 2*n-1)
	for i := range nodes {
		stringFromMinScore := make([]byte, dnaLen, dnaLen)
		for j := range parsimonyTrees {
			parsimonyTree := parsimonyTrees[j]
			charFromNode := parsimonyTree[i].min.char
			stringFromMinScore[j] = charFromNode
		}
		nodes[i] = &(node{i, string(stringFromMinScore), make(map[int]weightedEdge)})
	}

	//setting the relationships
	for i := 0; i < len(parsimonyTrees[0])-1; i++ {
		parsimonyNode := parsimonyTrees[0][i]
		if parsimonyNode.daughter != nil && parsimonyNode.son != nil {
			createBiEdge(nodes[i], nodes[parsimonyNode.daughter.id])
			createBiEdge(nodes[i], nodes[parsimonyNode.son.id])
		}
	}
	//setting the final two nodes:
	createBiEdge(nodes[len(nodes)-2], nodes[len(nodes)-3])

	return rosalindOutput{parsimonyScore, nodes[:len(nodes)-1]}
}

func createBiEdge(parent, child *node) {
	hd, _ := hammingDistance(parent.dnaString, child.dnaString)
	parent.neighbours[child.id] = weightedEdge{hd, child}
	child.neighbours[parent.id] = weightedEdge{hd, parent}
}

func hammingDistance(str1, str2 string) (int, error) {
	hd := 0
	if len(str1) != len(str2) {
		return 0, errors.New("str1 and str2 needs to be equal length")
	}
	for i := range str1 {
		char1 := str1[i]
		char2 := str2[i]
		if char1 != char2 {
			hd += 1
		}
	}
	return hd, nil
}

func createCharacterTree(rosalindInput *rosalindInput, index int) []*parsimonyNode {
	n := rosalindInput.numLeaves
	adjacencyList := rosalindInput.adjacencyList
	nodes := make([]*parsimonyNode, 2*n-1, 2*n-1)
	for i := range adjacencyList {
		edge := adjacencyList[i]
		if i < n { //leaf
			internalNodeIndex, _ := strconv.Atoi(edge.source)
			leafIndex := i
			leafChar := edge.destination[index]
			nodes[leafIndex] = &(parsimonyNode{leafIndex, false, leafChar, make(map[byte]int), minScore{leafChar, 0}, nil, nil})
			if nodes[internalNodeIndex] == nil {
				nodes[internalNodeIndex] = &(parsimonyNode{internalNodeIndex, false, 0, make(map[byte]int), minScore{}, nil, nil})
			}
			if nodes[internalNodeIndex].daughter == nil {
				nodes[internalNodeIndex].setDaughter(nodes[leafIndex])
			} else {
				nodes[internalNodeIndex].setSon(nodes[leafIndex])
			}
		} else { //internal node
			parentNodeIndex, _ := strconv.Atoi(edge.source)
			childNodeIndex, _ := strconv.Atoi(edge.destination)
			if nodes[childNodeIndex] == nil {
				nodes[childNodeIndex] = &(parsimonyNode{childNodeIndex, false, 0, make(map[byte]int), minScore{}, nil, nil})
			}
			if nodes[parentNodeIndex] == nil {
				nodes[parentNodeIndex] = &(parsimonyNode{parentNodeIndex, false, 0, make(map[byte]int), minScore{}, nil, nil})
			}
			if nodes[parentNodeIndex].daughter == nil {
				nodes[parentNodeIndex].setDaughter(nodes[childNodeIndex])
			} else {
				nodes[parentNodeIndex].setSon(nodes[childNodeIndex])
			}
		}
	}
	return nodes
}

func BacktrackTree(root *parsimonyNode) {
	//breadth-first traverse for generic binary tree:
	toBacktrack := []backTrackNode{backTrackNode{node: root, parentChar: 0}}
	for len(toBacktrack) != 0 {
		//pop
		visitingNode := toBacktrack[0].node
		parentChar := toBacktrack[0].parentChar
		toBacktrack = toBacktrack[1:len(toBacktrack)]

		//visit
		var minChar byte
		minScore := math.MaxInt16
		for char, score := range visitingNode.s {
			if score < minScore || (minScore == score && char == parentChar) {
				minScore = score
				minChar = char
			}
		}
		visitingNode.min.setChar(minChar)
		visitingNode.min.setScore(minScore)

		//appending child nodes
		if visitingNode.daughter != nil {
			toBacktrack = append(toBacktrack, backTrackNode{visitingNode.daughter, minChar})
		}
		if visitingNode.son != nil {
			toBacktrack = append(toBacktrack, backTrackNode{visitingNode.son, minChar})
		}
	}
}

//calculates s of all nodes, returns the root
func calculateS(nodes []*parsimonyNode, alphabet []byte) *parsimonyNode {
	//initialisation
	for i := range nodes {
		node := nodes[i]
		if node.char != 0 { //if a leaf:
			node.setTagged(true)
			for c := range alphabet {
				k := alphabet[c]
				if node.char == k {
					node.s[k] = 0
				} else {
					node.s[k] = math.MaxInt16
				}
			}
		} else {
			node.setTagged(false)
		}
	}
	//main loop
	var root *parsimonyNode
	ripeNode, err := getRipeNode(nodes)
	for err == nil {
		ripeNode.setTagged(true)
		for c := range alphabet {
			k := alphabet[c]
			ripeNode.s[k] = minOfSymbolsWithDelta(ripeNode.daughter.s, k) + minOfSymbolsWithDelta(ripeNode.son.s, k)
		}
		root = ripeNode
		ripeNode, err = getRipeNode(nodes)
	}

	return root
}

func minOfSymbolsWithDelta(s map[byte]int, k byte) int {
	minScoreWithDelta := math.MaxInt16
	for char, score := range s {
		var delta int
		if char == k {
			delta = 0
		} else {
			delta = 1
		}
		scoreWithDelta := score + delta
		if scoreWithDelta < minScoreWithDelta {
			minScoreWithDelta = scoreWithDelta
		}
	}
	return minScoreWithDelta
}

func getRipeNode(nodes []*parsimonyNode) (*parsimonyNode, error) {
	for i := range nodes {
		node := nodes[i]
		if node.tagged == false && node.daughter.tagged && node.son.tagged {
			return node, nil
		}
	}
	return nodes[0], errors.New("no ripe nodes left")
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func outputToConsole(formattedOutput []string) {
	for i := range formattedOutput {
		fmt.Println(formattedOutput[i])
	}
}

func formatOutput(rosalindOutput rosalindOutput) []string {
	var formattedOutput []string
	formattedOutput = append(formattedOutput, strconv.Itoa(rosalindOutput.parsimonyScore))
	for i := range rosalindOutput.nodes {
		node := rosalindOutput.nodes[i]
		source := node.dnaString
		for j := range node.neighbours {
			weight := node.neighbours[j].weight
			destination := node.neighbours[j].node.dnaString
			formattedOutput = append(formattedOutput, fmt.Sprintf("%s->%s:%d", source, destination, weight))
		}
	}
	return formattedOutput
}

func getInputFromSample() rosalindInput {
	n := 4
	adjacencyList := make([]rosalindInputEdge, 10, 10)
	adjacencyList[0] = rosalindInputEdge{"TCGGCCAA", "4"}
	adjacencyList[1] = rosalindInputEdge{"4", "TCGGCCAA"}
	adjacencyList[2] = rosalindInputEdge{"CCTGGCTG", "4"}
	adjacencyList[3] = rosalindInputEdge{"4", "CCTGGCTG"}
	adjacencyList[4] = rosalindInputEdge{"CACAGGAT", "5"}
	adjacencyList[5] = rosalindInputEdge{"5", "CACAGGAT"}
	adjacencyList[6] = rosalindInputEdge{"TGAGTACC", "5"}
	adjacencyList[7] = rosalindInputEdge{"5", "TGAGTACC"}
	adjacencyList[8] = rosalindInputEdge{"4", "5"}
	adjacencyList[9] = rosalindInputEdge{"5", "4"}
	return rosalindInput{n, adjacencyList}
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())
	var adjacencyList []rosalindInputEdge

	for scanner.Scan() {
		e := parseEdgeFromInput(scanner.Text())
		adjacencyList = append(adjacencyList, e)
	}

	return rosalindInput{numLeaves: n, adjacencyList: adjacencyList}
}

func parseEdgeFromInput(edge string) rosalindInputEdge {
	arrowSplit := strings.Split(edge, "->")
	source := arrowSplit[0]
	destination := arrowSplit[1]
	return rosalindInputEdge{source, destination}
}

//types

type backTrackNode struct {
	node       *parsimonyNode
	parentChar byte
}

type weightedEdge struct {
	weight int
	node   *node
}

type node struct {
	id         int
	dnaString  string
	neighbours map[int]weightedEdge //keep this generic, since the ultimate printout requires bidirectional edges
}

type parsimonyNode struct {
	id       int
	tagged   bool
	char     byte //only present in leaves
	s        map[byte]int
	min      minScore
	son      *parsimonyNode
	daughter *parsimonyNode
}

func (n *parsimonyNode) setSon(s *parsimonyNode) {
	n.son = s
}

func (n *parsimonyNode) setDaughter(d *parsimonyNode) {
	n.daughter = d
}

type minScore struct {
	char  byte
	score int
}

func (ms *minScore) setScore(s int) {
	ms.score = s
}

func (ms *minScore) setChar(c byte) {
	ms.char = c
}
func (p *parsimonyNode) setTagged(b bool) {
	p.tagged = b
}

type rosalindInputEdge struct {
	source      string
	destination string
}

type rosalindInput struct {
	numLeaves     int
	adjacencyList []rosalindInputEdge
}

type rosalindOutput struct {
	parsimonyScore int
	nodes          []*node
}

//helpers
func check(e error) {
	if e != nil {
		panic(e)
	}
}
