package nearestneighboursproblem

import (
	"testing"
)

func TestGetInputFromSample(t *testing.T) {
	input := getInputFromSample()
	expectedA := 5
	actualA := input.a
	if actualA != expectedA {
		t.Errorf("actualA: %d, expectedA: %d", actualA, expectedA)
	}
}

func TestGetLargestNodeId(t *testing.T) {
	ri := getInputFromSample()
	actualLargestId := getLargestNodeId(ri.adjacencyList)
	expectedLargestId := 5
	if actualLargestId != expectedLargestId {
		t.Errorf("actualLargestId: %d, expectedLargestId: %d", actualLargestId, expectedLargestId)
	}
}

func TestCreateTree(t *testing.T) {
	ri := getInputFromSample()
	tree := createTree(ri.adjacencyList)
	expectedLen := 6
	actualLen := len(tree)
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}
}

func TestGetIndicesToBeSwapped(t *testing.T) {
	ri := getInputFromSample()
	a := ri.a //5
	b := ri.b //4
	edges := ri.adjacencyList
	tree := createTree(edges)
	actualChildA, actualChildrenB := getIndicesToBeSwapped(tree, a, b)
	expectedChildA := 3
	expectedChildrenB := []int{0, 1}
	if actualChildA != expectedChildA {
		t.Errorf("actualChildA: %d, expectedChildA: %d", actualChildA, expectedChildA)
	}

	if actualChildrenB[0] != expectedChildrenB[0] {
		t.Errorf("actualChildrenB: %d, expectedChildrenB: %d", actualChildrenB, expectedChildrenB)
	}
}
