package nearestneighboursproblem

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	rosalindOutput := solve(input)
	formattedOutput := formatOutput(rosalindOutput)
	outputToConsole(formattedOutput)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	rosalindOutput := solve(input)
	formattedOutput := formatOutput(rosalindOutput)
	outputToFile(formattedOutput, outputPath)
}

var Solver = solver{}

//functions

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	ab := strings.Split(scanner.Text(), " ")
	a, _ := strconv.Atoi(ab[0])
	b, _ := strconv.Atoi(ab[1])
	var edges []edge

	for scanner.Scan() {
		e := parseEdgeFromInput(scanner.Text())
		edges = append(edges, e)
	}

	return rosalindInput{a, b, edges}
}

func parseEdgeFromInput(edgeStr string) edge {
	arrowSplit := strings.Split(edgeStr, "->")
	source, _ := strconv.Atoi(arrowSplit[0])
	destination, _ := strconv.Atoi(arrowSplit[1])
	return edge{source, destination}
}

func outputToConsole(formattedOutput []string) {
	for i := range formattedOutput {
		fmt.Println(formattedOutput[i])
	}
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func formatOutput(ro rosalindOutput) []string {
	var formattedOutput []string
	ns1 := ro.nearestNeighbour1
	for i := range ns1 {
		n := ns1[i]
		source := n.id
		for destination, _ := range n.neighbours {
			formattedOutput = append(formattedOutput, fmt.Sprintf("%d->%d", source, destination))
		}
	}

	formattedOutput = append(formattedOutput, "")

	ns2 := ro.nearestNeighbour2
	for i := range ns2 {
		n := ns2[i]
		source := n.id
		for destination, _ := range n.neighbours {
			formattedOutput = append(formattedOutput, fmt.Sprintf("%d->%d", source, destination))
		}
	}
	return formattedOutput
}

func getInputFromSample() rosalindInput {
	adjList := make([]edge, 10, 10)
	adjList[0] = edge{0, 4}
	adjList[1] = edge{4, 0}
	adjList[2] = edge{1, 4}
	adjList[3] = edge{4, 1}
	adjList[4] = edge{2, 5}
	adjList[5] = edge{5, 2}
	adjList[6] = edge{3, 5}
	adjList[7] = edge{5, 3}
	adjList[8] = edge{4, 5}
	adjList[9] = edge{5, 4}
	return rosalindInput{5, 4, adjList}
}

func solve(ri rosalindInput) rosalindOutput {
	a := ri.a
	b := ri.b
	edges := ri.adjacencyList
	tree := createTree(edges)
	ro := createNearestNeighbours(tree, a, b)
	return ro
}

func createNearestNeighbours(tree []*node, a, b int) rosalindOutput {
	childA, childrenB := getIndicesToBeSwapped(tree, a, b)
	nearestNeighbour1 := swapChildren(tree, childA, a, childrenB[0], b)
	nearestNeighbour2 := swapChildren(tree, childA, a, childrenB[1], b)
	return rosalindOutput{nearestNeighbour1, nearestNeighbour2}
}

func getIndicesToBeSwapped(tree []*node, a, b int) (int, []int) {
	//take any child in A
	var childA int
	for id, _ := range tree[a].neighbours {
		if id != b {
			childA = id
		}
	}

	childrenB := make([]int, 2, 2)
	indexB := 0
	for id, _ := range tree[b].neighbours {
		if id != a {
			childrenB[indexB] = id
			indexB += 1
		}
	}
	return childA, childrenB
}

func swapChildren(tree []*node, child1, parent1, child2, parent2 int) []*node {
	treeLen := len(tree)
	newTree := make([]*node, treeLen, treeLen)

	//creating nodes
	for i := range tree {
		newId := tree[i].id
		newNeighbour := make(map[int]*node)
		newTree[i] = &(node{newId, newNeighbour})
	}
	//adding edges
	for i := range tree {
		for destinationId, _ := range tree[i].neighbours {
			newTree[i].neighbours[destinationId] = newTree[destinationId]
		}
		delete(newTree[child1].neighbours, parent1)
		delete(newTree[parent1].neighbours, child1)
		delete(newTree[child2].neighbours, parent2)
		delete(newTree[parent2].neighbours, child2)
		newTree[child2].neighbours[parent1] = newTree[parent1]
		newTree[parent1].neighbours[child2] = newTree[child2]
		newTree[child1].neighbours[parent2] = newTree[parent2]
		newTree[parent2].neighbours[child1] = newTree[child1]
	}
	return newTree
}

func createTree(edges []edge) []*node {
	n := getLargestNodeId(edges)
	tree := make([]*node, n+1, n+1)
	for i := range edges {
		source := edges[i].source
		destination := edges[i].destination

		if tree[source] == nil {
			tree[source] = &(node{source, make(map[int]*node)})
		}
		if tree[destination] == nil {
			tree[destination] = &(node{destination, make(map[int]*node)})
		}
		tree[source].neighbours[destination] = tree[destination]
	}
	return tree
}

func getLargestNodeId(edges []edge) int {
	largestInt := 0
	for i := 0; i < len(edges); i += 2 {
		if edges[i].source > largestInt {
			largestInt = edges[i].source
		}
		if edges[i].destination > largestInt {
			largestInt = edges[i].destination
		}
	}
	return largestInt
}

// types
type node struct {
	id         int
	neighbours map[int]*node
}

type rosalindOutput struct {
	nearestNeighbour1 []*node
	nearestNeighbour2 []*node
}

type rosalindInput struct {
	a             int
	b             int
	adjacencyList []edge
}

type edge struct {
	source      int
	destination int
}

//helpers
func check(e error) {
	if e != nil {
		panic(e)
	}
}
