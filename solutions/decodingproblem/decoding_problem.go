package decodingproblem

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

var Solver = solver{}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	emittedString := solve(input)
	fmt.Println(emittedString)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	emittedString := solve(input)
	outputToFile([]string{emittedString}, outputPath)
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func solve(ri rosalindInput) string {
	// solving using the Viterbi algorithm, using sum of logarithms

	//initialisation:
	emittedString := ri.emittedString
	alphabet := ri.alphabet
	letterToIndex := make(map[byte]int)
	for index, value := range alphabet {
		letterToIndex[value] = index
	}
	states := ri.states
	numStates := len(states)
	stateToIndex := make(map[byte]int)
	for index, value := range states {
		stateToIndex[value] = index
	}
	transition := ri.transition
	emission := ri.emission

	pathProbMatrix := make([][]float64, len(emittedString), len(emittedString))
	for i, _ := range pathProbMatrix {
		pathProbMatrix[i] = make([]float64, numStates, numStates)
	}
	for j, _ := range pathProbMatrix[0] {
		pathProbMatrix[0][j] = math.Log2(0.5) +
			math.Log2(float64(emission[j][letterToIndex[emittedString[0]]]))
	}

	//backtrace matrix elements initialised at -1
	backtraceMatrix := make([][]int, len(emittedString), len(emittedString))
	for i, _ := range backtraceMatrix {
		backtraceMatrix[i] = make([]int, numStates, numStates)
		for j, _ := range backtraceMatrix[i] {
			backtraceMatrix[i][j] = -1
		}
	}

	//dynamic programming part
	for i := 1; i < len(emittedString); i++ {
		for j, _ := range pathProbMatrix[i] {
			transEmissions := make([]float64, numStates, numStates)
			for k, prevProb := range pathProbMatrix[i-1] {
				transEmissions[k] = prevProb +
					math.Log2(float64(transition[k][j])) +
					math.Log2(float64(emission[j][letterToIndex[emittedString[i]]]))
			}
			maxStateIndex, maxProbability := maxFloat64(transEmissions)
			pathProbMatrix[i][j] = maxProbability
			backtraceMatrix[i][j] = maxStateIndex
		}
	}

	//initialisation for backtracking
	maxStateIndex, _ := maxFloat64(pathProbMatrix[len(emittedString)-1])
	accumulatedPaths := []byte{states[maxStateIndex]}

	// backtrack starts
	for i := len(emittedString) - 1; i > 0; i-- {
		maxStateIndex := backtraceMatrix[i][maxStateIndex]
		accumulatedPaths = append(accumulatedPaths, states[maxStateIndex])
	}

	return reverseBytes(string(accumulatedPaths))
}

func getInputFromSample() rosalindInput {
	emittedString := "xyxzzxyxyy"
	alphabet := []byte{'x', 'y', 'z'}
	states := []byte{'A', 'B'}
	transition := make([][]float32, 2)
	transition[0] = []float32{0.641, 0.359}
	transition[1] = []float32{0.729, 0.271}
	emission := make([][]float32, 2)
	emission[0] = []float32{0.117, 0.691, 0.192}
	emission[1] = []float32{0.097, 0.42, 0.483}
	return rosalindInput{emittedString, alphabet, states, transition, emission}
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	emittedString := scanner.Text()

	scanner.Scan() //-------

	scanner.Scan()
	alphabetstr := scanner.Text()
	alphabetstrs := strings.Split(alphabetstr, "\t")
	alphabet := make([]byte, len(alphabetstrs), len(alphabetstrs))
	for i, value := range alphabetstrs {
		alphabet[i] = value[0]
	}

	scanner.Scan() //-------

	scanner.Scan()
	statesStr := scanner.Text()
	stateStrs := strings.Split(statesStr, "\t")
	states := make([]byte, len(stateStrs), len(stateStrs))
	for i, value := range stateStrs {
		states[i] = value[0]
	}

	scanner.Scan() //-------
	scanner.Scan() // A B

	transitions := make([][]float32, len(states), len(states))
	for rowNum := 0; rowNum < len(states); rowNum++ {
		scanner.Scan()
		transitionRowStr := strings.Split(scanner.Text(), "\t")[1:]
		transitions[rowNum] = make([]float32, len(states))
		for i := 0; i < len(states); i++ {
			transitionElem, _ := strconv.ParseFloat(transitionRowStr[i], 32)
			transitions[rowNum][i] = float32(transitionElem)
		}
	}

	scanner.Scan() //-------
	scanner.Scan() // x y z

	emissions := make([][]float32, len(states), len(states))
	for rowNum := 0; rowNum < len(states); rowNum++ {
		scanner.Scan()
		emissionRowStr := strings.Split(scanner.Text(), "\t")[1:]
		emissions[rowNum] = make([]float32, len(alphabet))
		for i := 0; i < len(alphabet); i++ {
			emissionElem, _ := strconv.ParseFloat(emissionRowStr[i], 32)
			emissions[rowNum][i] = float32(emissionElem)
		}
	}

	return rosalindInput{emittedString, alphabet, states, transitions, emissions}
}

type rosalindInput struct {
	emittedString string
	alphabet      []byte
	states        []byte
	transition    [][]float32
	emission      [][]float32
}

//helpers
func maxFloat64(floats []float64) (int, float64) {
	index := 0
	max := floats[0]
	for i, value := range floats {
		if value > max {
			max = value
			index = i
		}
	}
	return index, max
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

//From: http://rosettacode.org/wiki/Reverse_a_string#Go
func reverseBytes(s string) string {
	r := make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		r[i] = s[len(s)-1-i]
	}
	return string(r)
}
