package decodingproblem

import (
	"testing"
)

func TestSolveFromSample(t *testing.T) {
	input := getInputFromSample()
	actualString := solve(input)
	expectedString := "AAABBAAAAA"
	if actualString != expectedString {
		t.Errorf("actualString: %s, expectedString: %s", actualString, expectedString)
	}
}
