package largeparsimonyproblem

import (
	"bufio"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

//exposed

type solver struct {
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	rosalindOutputs := solve(input)
	formattedOutput := formatOutput(rosalindOutputs)
	outputToConsole(formattedOutput)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	rosalindOutputs := solve(input)
	formattedOutput := formatOutput(rosalindOutputs)
	outputToFile(formattedOutput, outputPath)
}

var Solver = solver{}

//internals

func solve(ri rosalindInput) []rosalindOutput {
	//initial solve
	var ros []rosalindOutput
	ro := solveUnrootedSmallParsimony(ri)
	ros = append(ros, ro)

	//iteratively now
	riIt, roIt, foundSmallerParsimony := solveUnrootedSmallParsimonyGreedy(ri, ro.parsimonyScore)
	for foundSmallerParsimony {
		ros = append(ros, roIt)
		riIt, roIt, foundSmallerParsimony = solveUnrootedSmallParsimonyGreedy(riIt, roIt.parsimonyScore)
	}
	return ros
}

func solveUnrootedSmallParsimonyGreedy(ri rosalindInput, currentScore int) (rosalindInput, rosalindOutput, bool) {
	internalNodess := generateInternalNodes(ri)
	bestScore := math.MaxInt16
	var bestInterchangeInput rosalindInput
	var bestInterchangeOutput rosalindOutput
	for i := range internalNodess {
		ins := internalNodess[i]
		riSwappeds := swapWithNearestNeighbours(ri, ins)
		for j := range riSwappeds {
			riSwapped := riSwappeds[j]
			ri := convertToRosalindInput(riSwapped, ri.numLeaves)
			riRooted := convertToIntermediateInput(ri)
			ro := solveSmallParsimony(riRooted)
			if ro.parsimonyScore < bestScore {
				bestScore = ro.parsimonyScore
				bestInterchangeInput = ri
				bestInterchangeOutput = ro
			}
		}
	}
	if bestScore < currentScore {
		return bestInterchangeInput, bestInterchangeOutput, true
	}
	return rosalindInput{}, rosalindOutput{}, false
}

func convertToRosalindInput(nnnodes map[string]*nnNode, numLeaves int) rosalindInput {
	var adjacencyList []rosalindInputEdge
	for sourceId, sourceNode := range nnnodes {
		for destinationId, _ := range sourceNode.neighbours {
			adjacencyList = append(adjacencyList, rosalindInputEdge{
				source:      sourceId,
				destination: destinationId,
			})
		}
	}
	return rosalindInput{
		numLeaves:     numLeaves,
		adjacencyList: adjacencyList,
	}
}

func swapWithNearestNeighbours(ri rosalindInput, ins internalNodes) []map[string]*nnNode {
	parentA := strconv.Itoa(ins.nodeId1)
	parentB := strconv.Itoa(ins.nodeId2)
	ris := make([]map[string]*nnNode, 2, 2)
	tree := createNNTree(ri.adjacencyList)
	childA, childrenB := getIndicesToBeSwapped(tree, parentA, parentB)

	ris[0] = swapChildren(tree, childA, parentA, childrenB[0], parentB)
	ris[1] = swapChildren(tree, childA, parentA, childrenB[1], parentB)
	return ris
}

func swapChildren(tree map[string]*nnNode, child1, parent1, child2, parent2 string) map[string]*nnNode {
	newTree := make(map[string]*nnNode)

	//creating nodes
	for id, _ := range tree {
		newNeighbour := make(map[string]*nnNode)
		newTree[id] = &(nnNode{id, newNeighbour})
	}
	//adding edges
	for id, _ := range tree {
		for destinationId, _ := range tree[id].neighbours {
			newTree[id].neighbours[destinationId] = newTree[destinationId]
		}
	}

	delete(newTree[child1].neighbours, parent1)
	delete(newTree[parent1].neighbours, child1)
	delete(newTree[child2].neighbours, parent2)
	delete(newTree[parent2].neighbours, child2)
	newTree[child2].neighbours[parent1] = newTree[parent1]
	newTree[parent1].neighbours[child2] = newTree[child2]
	newTree[child1].neighbours[parent2] = newTree[parent2]
	newTree[parent2].neighbours[child1] = newTree[child1]

	return newTree
}

func getIndicesToBeSwapped(tree map[string]*nnNode, a, b string) (string, []string) {

	var childA string
	for id, _ := range tree[a].neighbours {
		if id != b {
			childA = id
		}
	}

	childrenB := make([]string, 2, 2)
	indexB := 0
	for id, _ := range tree[b].neighbours {
		if id != a {
			childrenB[indexB] = id
			indexB += 1
		}
	}
	return childA, childrenB
}

func createNNTree(edges []rosalindInputEdge) map[string]*nnNode {
	tree := make(map[string]*nnNode)
	for i := range edges {
		source := edges[i].source
		destination := edges[i].destination

		if tree[source] == nil {
			tree[source] = &(nnNode{source, make(map[string]*nnNode)})
		}
		if tree[destination] == nil {
			tree[destination] = &(nnNode{destination, make(map[string]*nnNode)})
		}
		tree[source].neighbours[destination] = tree[destination]
	}
	return tree
}

func getLargestNodeId(edges []rosalindInputEdge) int {
	largestInt := 0
	for i := 0; i < len(edges); i++ {
		edge := edges[i]
		sourceInt, convertedSource := strconv.Atoi(edge.source)
		destinationInt, convertedDestination := strconv.Atoi(edge.destination)
		if convertedSource == nil && sourceInt > largestInt {
			largestInt = sourceInt
		}
		if convertedDestination == nil && destinationInt > largestInt {
			largestInt = destinationInt
		}
	}
	return largestInt
}

func generateInternalNodes(ri rosalindInput) []internalNodes {
	var internalNodess []internalNodes
	added := make(map[string]bool)
	for i := range ri.adjacencyList {
		edge := ri.adjacencyList[i]
		sourceId, convertedSource := strconv.Atoi(edge.source)
		destinationId, convertedDestination := strconv.Atoi(edge.destination)
		if convertedSource == nil && convertedDestination == nil {
			hash1 := edge.source + edge.destination
			hash2 := edge.destination + edge.source
			_, present := added[hash1]
			if !present {
				internalNodess = append(internalNodess, internalNodes{nodeId1: sourceId, nodeId2: destinationId})
			}
			added[hash1] = true
			added[hash2] = true
		}
	}
	return internalNodess
}

func solveUnrootedSmallParsimony(ri rosalindInput) rosalindOutput {
	riRooted := convertToIntermediateInput(ri)
	ro := solveSmallParsimony(riRooted)
	return ro
}

func solveSmallParsimony(riRooted rosalindIntermediateInput) rosalindOutput {
	dnaLen := riRooted.dnaLen
	n := riRooted.numLeaves
	rootId := riRooted.rootId

	parsimonyTrees := make([][]*parsimonyNode, dnaLen, dnaLen)
	alphabet := []byte{'A', 'C', 'T', 'G'}
	_ = alphabet
	parsimonyScore := 0

	for i := 0; i < dnaLen; i++ {
		parsimonyTrees[i] = createCharacterTree(&riRooted, i)
		root := calculateS(parsimonyTrees[i], alphabet)
		BacktrackTree(root)
		parsimonyScore += root.min.score
	}

	//put all but the last one in
	nodes := make([]*node, 2*n-1, 2*n-1)
	for i := range nodes {
		stringFromMinScore := make([]byte, dnaLen, dnaLen)
		for j := range parsimonyTrees {
			parsimonyTree := parsimonyTrees[j]
			charFromNode := parsimonyTree[i].min.char
			stringFromMinScore[j] = charFromNode
		}
		nodes[i] = &(node{i, string(stringFromMinScore), make(map[int]weightedEdge)})
	}

	//setting the relationships
	for i := 0; i < len(parsimonyTrees[0])-1; i++ {
		parsimonyNode := parsimonyTrees[0][i]
		if parsimonyNode.daughter != nil && parsimonyNode.son != nil {
			createBiEdge(nodes[i], nodes[parsimonyNode.daughter.id])
			createBiEdge(nodes[i], nodes[parsimonyNode.son.id])
		}
	}

	//adding in relationships for original internal nodes:
	internalNode1 := nodes[riRooted.nodes[rootId].daughter.id]
	internalNode2 := nodes[riRooted.nodes[rootId].son.id]
	createBiEdge(internalNode1, internalNode2)

	return rosalindOutput{
		parsimonyScore: parsimonyScore,
		nodes:          nodes[:len(nodes)-1],
	}
}

func createBiEdge(parent, child *node) {
	hd, _ := hammingDistance(parent.dnaString, child.dnaString)
	parent.neighbours[child.id] = weightedEdge{hd, child}
	child.neighbours[parent.id] = weightedEdge{hd, parent}
}

func calculateS(nodes []*parsimonyNode, alphabet []byte) *parsimonyNode {
	//initialisation
	for i := range nodes {
		node := nodes[i]
		if node.char != 0 { //if a leaf:
			node.setTagged(true)
			for c := range alphabet {
				k := alphabet[c]
				if node.char == k {
					node.s[k] = 0
				} else {
					node.s[k] = math.MaxInt16
				}
			}
		} else {
			node.setTagged(false)
		}
	}
	//main loop
	var root *parsimonyNode
	ripeNode, err := getRipeNode(nodes)
	for err == nil {
		ripeNode.setTagged(true)
		for c := range alphabet {
			k := alphabet[c]
			ripeNode.s[k] = minOfSymbolsWithDelta(ripeNode.daughter.s, k) + minOfSymbolsWithDelta(ripeNode.son.s, k)
		}
		root = ripeNode
		ripeNode, err = getRipeNode(nodes)
	}

	return root
}

func minOfSymbolsWithDelta(s map[byte]int, k byte) int {
	minScoreWithDelta := math.MaxInt16
	for char, score := range s {
		var delta int
		if char == k {
			delta = 0
		} else {
			delta = 1
		}
		scoreWithDelta := score + delta
		if scoreWithDelta < minScoreWithDelta {
			minScoreWithDelta = scoreWithDelta
		}
	}
	return minScoreWithDelta
}

func getRipeNode(nodes []*parsimonyNode) (*parsimonyNode, error) {
	for i := range nodes {
		node := nodes[i]
		if node.tagged == false && node.daughter.tagged && node.son.tagged {
			return node, nil
		}
	}
	return nodes[0], errors.New("no ripe nodes left")
}

func BacktrackTree(root *parsimonyNode) {
	//breadth-first traverse for generic binary tree:
	toBacktrack := []backTrackNode{backTrackNode{node: root, parentChar: 0}}
	for len(toBacktrack) != 0 {
		//pop
		visitingNode := toBacktrack[0].node
		parentChar := toBacktrack[0].parentChar
		toBacktrack = toBacktrack[1:len(toBacktrack)]

		//visit
		var minChar byte
		minScore := math.MaxInt16
		for char, score := range visitingNode.s {
			if score < minScore || (minScore == score && char == parentChar) {
				minScore = score
				minChar = char
			}
		}
		visitingNode.min.setChar(minChar)
		visitingNode.min.setScore(minScore)

		//appending child nodes
		if visitingNode.daughter != nil {
			toBacktrack = append(toBacktrack, backTrackNode{visitingNode.daughter, minChar})
		}
		if visitingNode.son != nil {
			toBacktrack = append(toBacktrack, backTrackNode{visitingNode.son, minChar})
		}
	}
}

func assignIntId(nodes map[string]*intermediateInputNode) int {
	indexCounter := 0
	rootId := 0
	for nodeId, iNode := range nodes {
		leafIndex, err := strconv.Atoi(nodeId)
		if err != nil { //leaf
			iNode.setAssignedId(indexCounter) //use custom counter instead
			indexCounter += 1
		} else { //internal node
			iNode.setAssignedId(leafIndex)
			if leafIndex > rootId {
				rootId = leafIndex
			}
		}
	}
	return rootId + 1
}

func createCharacterTree(rio *rosalindIntermediateInput, index int) []*parsimonyNode {
	n := rio.numLeaves
	nodes := make([]*parsimonyNode, 2*n-1, 2*n-1)

	//creating the nodes
	for id, iNode := range rio.nodes {
		if id < n { //leaf
			leafChar := iNode.dnaString[index]
			nodes[id] = &(parsimonyNode{id, false, leafChar, make(map[byte]int), minScore{leafChar, 0}, nil, nil})
		} else { //internalNode
			nodes[id] = &(parsimonyNode{id, false, 0, make(map[byte]int), minScore{}, nil, nil})
		}
	}

	//adding the relationships
	for id, iNode := range rio.nodes {
		pNode := nodes[id]
		if iNode.daughter != nil {
			pNode.setDaughter(nodes[iNode.daughter.id])
		}
		if iNode.son != nil {
			pNode.setSon(nodes[iNode.son.id])
		}
	}

	return nodes
}

func convertToIntermediateInput(ri rosalindInput) rosalindIntermediateInput {
	numLeaves := ri.numLeaves
	adjacencyList := ri.adjacencyList
	nodes := make(map[string]*intermediateInputNode)
	var intermediateNode1 *intermediateInputNode
	var intermediateNode2 *intermediateInputNode
	var dnaLen int

	for i := range adjacencyList {
		source := adjacencyList[i].source
		destination := adjacencyList[i].destination

		//populating if not there
		_, ok := nodes[source]
		if !ok {
			nodes[source] = &(intermediateInputNode{id: source, neighbours: make(map[string]*intermediateInputNode)})
		}
		_, ok = nodes[destination]
		if !ok {
			nodes[destination] = &(intermediateInputNode{id: destination, neighbours: make(map[string]*intermediateInputNode)})
		}

		nodes[source].neighbours[destination] = nodes[destination]

		//picking out edges to insert the root
		_, err1 := strconv.Atoi(source)
		_, err2 := strconv.Atoi(destination)
		if intermediateNode1 == nil && intermediateNode2 == nil && err1 == nil && err2 == nil {
			intermediateNode1 = nodes[source]
			intermediateNode2 = nodes[destination]
		}

		if len(source) > dnaLen {
			dnaLen = len(source)
		}
		if len(destination) > dnaLen {
			dnaLen = len(destination)
		}
	}

	//Converting to a rooted tree
	rootId := assignIntId(nodes)
	strRootId := strconv.Itoa(rootId)
	nodes[strRootId] = &(intermediateInputNode{id: strRootId, assignedId: rootId, neighbours: make(map[string]*intermediateInputNode)})

	nodes[strRootId].neighbours[intermediateNode1.id] = intermediateNode1
	nodes[strRootId].neighbours[intermediateNode2.id] = intermediateNode2
	delete(intermediateNode1.neighbours, intermediateNode2.id)
	delete(intermediateNode2.neighbours, intermediateNode1.id)

	rootedNodes := createRootedNodes(&nodes, rootId)

	return rosalindIntermediateInput{
		rootId:    rootId,
		nodes:     rootedNodes,
		numLeaves: numLeaves,
		dnaLen:    dnaLen,
	}
}

func createRootedNodes(nodes *map[string]*intermediateInputNode, rootId int) map[int]*rootedInputNode {
	rootedNodes := make(map[int]*rootedInputNode)
	//creating the nodes
	for nodeId, iNode := range *nodes {
		var dnaString string
		id := iNode.assignedId
		_, err := strconv.Atoi(nodeId)
		if err != nil { //int is actually a leaf
			dnaString = nodeId
		}
		rootedNodes[id] = &(rootedInputNode{
			id:        id,
			dnaString: dnaString,
		})
	}
	//creating the relationships, from the root
	visited := make(map[int]bool)
	toVisit := []*intermediateInputNode{(*nodes)[strconv.Itoa(rootId)]}
	for len(toVisit) != 0 {
		//pop
		currentNode := toVisit[0]
		toVisit = toVisit[1:len(toVisit)]

		//visiting:
		visited[currentNode.assignedId] = true
		rootedNode := rootedNodes[currentNode.assignedId]
		for _, iNode := range currentNode.neighbours {
			realId := iNode.assignedId
			neighbourNode := rootedNodes[realId]
			//add relationship if it hasn't been added
			if _, present := visited[realId]; !present {
				if rootedNode.daughter == nil {
					rootedNode.setDaughter(neighbourNode)
					toVisit = append(toVisit, iNode)
				} else if rootedNode.son == nil {
					rootedNode.setSon(neighbourNode)
					toVisit = append(toVisit, iNode)
				} else {
					errors.New("needs to be a binary tree")
				}
			}
		}
	}
	//printTree(rootedNodes[rootId])
	return rootedNodes
}

func printTree(root *rootedInputNode) {
	toVisit := []*rootedInputNode{root}
	for len(toVisit) != 0 {
		//pop
		currentNode := toVisit[0]
		toVisit = toVisit[1:len(toVisit)]

		//visiting
		fmt.Printf("visiting %d: %s\n", currentNode.id, currentNode.dnaString)

		//appending
		if currentNode.daughter != nil {
			toVisit = append(toVisit, currentNode.daughter)
		}

		if currentNode.son != nil {
			toVisit = append(toVisit, currentNode.son)
		}
	}
}

func max(a, b int) int {
	if a < b {
		return b
	}
	return a
}

//boring IO

func formatOutput(ros []rosalindOutput) []string {
	var formattedOutput []string
	for i := range ros {
		ro := ros[i]
		formattedOutput = append(formattedOutput, strconv.Itoa(ro.parsimonyScore))
		for i := range ro.nodes {
			node := ro.nodes[i]
			source := node.dnaString
			for j := range node.neighbours {
				weight := node.neighbours[j].weight
				destination := node.neighbours[j].node.dnaString
				formattedOutput = append(formattedOutput, fmt.Sprintf("%s->%s:%d", source, destination, weight))
			}
		}
		formattedOutput = append(formattedOutput, "")
	}
	return formattedOutput
}

func outputToConsole(strs []string) {
	for i := range strs {
		fmt.Println(strs[i])
	}
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())
	var adjacencyList []rosalindInputEdge

	for scanner.Scan() {
		e := parseEdgeFromInput(scanner.Text())
		adjacencyList = append(adjacencyList, e)
	}

	return rosalindInput{numLeaves: n, adjacencyList: adjacencyList}
}

func parseEdgeFromInput(edge string) rosalindInputEdge {
	arrowSplit := strings.Split(edge, "->")
	source := arrowSplit[0]
	destination := arrowSplit[1]
	return rosalindInputEdge{source, destination}
}

func getInputFromSample() rosalindInput {
	n := 4
	adjacencyList := make([]rosalindInputEdge, 10, 10)

	adjacencyList[0] = rosalindInputEdge{"CGAAGATTCTAA", "4"}
	adjacencyList[1] = rosalindInputEdge{"ATGCCGGGCTCG", "4"}
	adjacencyList[2] = rosalindInputEdge{"CTTTTAGAAGCG", "5"}
	adjacencyList[3] = rosalindInputEdge{"AACTCATGATAT", "5"}
	adjacencyList[4] = rosalindInputEdge{"5", "AACTCATGATAT"}
	adjacencyList[5] = rosalindInputEdge{"5", "CTTTTAGAAGCG"}
	adjacencyList[6] = rosalindInputEdge{"5", "4"}
	adjacencyList[7] = rosalindInputEdge{"4", "ATGCCGGGCTCG"}
	adjacencyList[8] = rosalindInputEdge{"4", "CGAAGATTCTAA"}
	adjacencyList[9] = rosalindInputEdge{"4", "5"}

	return rosalindInput{n, adjacencyList}
}

//types

type nnNode struct {
	id         string
	neighbours map[string]*nnNode
}

type backTrackNode struct {
	node       *parsimonyNode
	parentChar byte
}

type weightedEdge struct {
	weight int
	node   *node
}

type node struct {
	id         int
	dnaString  string
	neighbours map[int]weightedEdge //keep this generic, since the ultimate printout requires bidirectional edges
}

type rootedInputNode struct {
	id        int
	dnaString string
	son       *rootedInputNode
	daughter  *rootedInputNode
}

func (n *rootedInputNode) setSon(s *rootedInputNode) {
	n.son = s
}

func (n *rootedInputNode) setDaughter(d *rootedInputNode) {
	n.daughter = d
}

type parsimonyNode struct {
	id       int
	tagged   bool
	char     byte //only present in leaves
	s        map[byte]int
	min      minScore
	son      *parsimonyNode
	daughter *parsimonyNode
}

func (n *parsimonyNode) setSon(s *parsimonyNode) {
	n.son = s
}

func (n *parsimonyNode) setDaughter(d *parsimonyNode) {
	n.daughter = d
}

type minScore struct {
	char  byte
	score int
}

func (ms *minScore) setScore(s int) {
	ms.score = s
}

func (ms *minScore) setChar(c byte) {
	ms.char = c
}
func (p *parsimonyNode) setTagged(b bool) {
	p.tagged = b
}

type rosalindInputEdge struct {
	source      string
	destination string
}

type rosalindInput struct {
	numLeaves     int
	adjacencyList []rosalindInputEdge
}

type rosalindIntermediateInput struct {
	rootId    int
	numLeaves int
	nodes     map[int]*rootedInputNode
	dnaLen    int
}

type intermediateInputNode struct {
	id         string
	assignedId int
	neighbours map[string]*intermediateInputNode
}

func (i *intermediateInputNode) setAssignedId(id int) {
	i.assignedId = id
}

type rosalindOutput struct {
	parsimonyScore int
	nodes          []*node
}

type internalNodes struct {
	nodeId1 int
	nodeId2 int
}

func hammingDistance(str1, str2 string) (int, error) {
	hd := 0
	if len(str1) != len(str2) {
		return 0, errors.New("str1 and str2 needs to be equal length")
	}
	for i := range str1 {
		char1 := str1[i]
		char2 := str2[i]
		if char1 != char2 {
			hd += 1
		}
	}
	return hd, nil
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
