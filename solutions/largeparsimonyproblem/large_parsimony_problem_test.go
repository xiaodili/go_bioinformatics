package largeparsimonyproblem

import (
	"path/filepath"
	"testing"
)

func TestSolveUnrootedSmallParsimony(t *testing.T) {
	ri := getInputFromSample()
	ro := solveUnrootedSmallParsimony(ri)
	actualParsimony := ro.parsimonyScore
	expectedParsimony := 22
	if actualParsimony != expectedParsimony {
		t.Errorf("actualParsimony: %d, expectedParsimony: %d", actualParsimony, expectedParsimony)
	}

	actualNodeLen := len(ro.nodes)
	expectedNodeLen := 6

	if actualNodeLen != expectedNodeLen {
		t.Errorf("actualNodeLen: %d, expectedNodeLen: %d", actualNodeLen, expectedNodeLen)
	}

}

func TestSolveUnrootedSmallParsimonyFromFile(t *testing.T) {
	path, _ := filepath.Abs("../../assets/rosalind_sample.txt")
	ri := getInputFromFile(path)
	ro := solveUnrootedSmallParsimony(ri)
	actualParsimony := ro.parsimonyScore
	expectedParsimony := 340
	if actualParsimony != expectedParsimony {
		t.Errorf("actualParsimony: %d, expectedParsimony: %d", actualParsimony, expectedParsimony)
	}
}

func TestConvertToIntermediateInput(t *testing.T) {
	ri := getInputFromSample()
	rii := convertToIntermediateInput(ri)

	actualDnaLen := rii.dnaLen
	expectedDnaLen := 12
	if actualDnaLen != expectedDnaLen {
		t.Errorf("actualDnaLen: %d, expectedDnaLen: %d", actualDnaLen, expectedDnaLen)
	}

	actualRootId := rii.rootId
	expectedRootId := 6
	if actualRootId != expectedRootId {
		t.Errorf("actualRootId: %d, expectedRootId: %d", actualRootId, expectedRootId)
	}

	actualNodeLen := len(rii.nodes)
	expectedNodeLen := 7
	if actualNodeLen != expectedNodeLen {
		t.Errorf("actualNodeLen: %d, expectedNodeLen: %d", actualNodeLen, expectedNodeLen)
	}
}

func TestGenerateInternalNodes(t *testing.T) {
	ri := getInputFromSample()
	ins := generateInternalNodes(ri)
	expectedLen := 1
	actualLen := len(ins)
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}
}

func TestGenerateInternalNodesLarger(t *testing.T) {
	path, _ := filepath.Abs("../../assets/rosalind_sample.txt")
	ri := getInputFromFile(path)
	ins := generateInternalNodes(ri)
	expectedLen := 13
	actualLen := len(ins)
	if actualLen != expectedLen {
		t.Errorf("actualLen: %d, expectedLen: %d", actualLen, expectedLen)
	}
}

func TestRosalindOutputCheck(t *testing.T) {
	ri := getInputFromSample()
	rii := convertToIntermediateInput(ri)
	ro := solveSmallParsimony(rii)
	ros := []rosalindOutput{ro}
	fo := formatOutput(ros)
	outputToConsole(fo)
}

func TestSolveFromSample(t *testing.T) {
	input := getInputFromSample()
	rosalindOutputs := solve(input)
	formattedOutput := formatOutput(rosalindOutputs)
	outputToConsole(formattedOutput)
}
