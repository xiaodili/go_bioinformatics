package distancebetweenleavesproblem

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type weightedEdge struct {
	source      int
	destination int
	weight      int
}

type rosalindInput struct {
	n             int
	adjacencyList []weightedEdge
}

func parseWeightedEdge(str string) weightedEdge {
	arrowSplit := strings.Split(str, "->")
	colonSplit := strings.Split(arrowSplit[1], ":")
	source, _ := strconv.Atoi(arrowSplit[0])
	destination, _ := strconv.Atoi(colonSplit[0])
	weight, _ := strconv.Atoi(colonSplit[1])
	return weightedEdge{source, destination, weight}
}

func getInputFromFile(path string) rosalindInput {
	inputFile, err := os.Open(path)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())

	var weightedEdges []weightedEdge
	for scanner.Scan() {
		ew := parseWeightedEdge(scanner.Text())
		weightedEdges = append(weightedEdges, ew)
	}
	return rosalindInput{n, weightedEdges}
}

func getInputFromSample() rosalindInput {
	n := 4
	var weightedEdges = []weightedEdge{
		weightedEdge{0, 4, 11},
		weightedEdge{1, 4, 2},
		weightedEdge{2, 5, 6},
		weightedEdge{3, 5, 7},
		weightedEdge{4, 0, 11},
		weightedEdge{4, 1, 2},
		weightedEdge{4, 5, 4},
		weightedEdge{5, 4, 4},
		weightedEdge{5, 3, 7},
		weightedEdge{5, 2, 6},
	}
	return rosalindInput{n, weightedEdges}
}

type weightDestination struct {
	weight      int
	destination int
}

type leafDistance struct {
	leaf     int
	distance int
}

type travelledDistance struct {
	node     int
	distance int
}

func solve(rosalindInput rosalindInput) [][]int {
	n := rosalindInput.n

	//initialising distanceMatrix to be returning
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}

	//creating the map for edge lookup
	node2Edges := make(map[int][]weightDestination)
	for _, element := range rosalindInput.adjacencyList {
		elem, ok := node2Edges[element.source]
		if ok {
			node2Edges[element.source] = append(elem, weightDestination{element.weight, element.destination})
		} else {
			node2Edges[element.source] = []weightDestination{weightDestination{element.weight, element.destination}}
		}
	}

	for i := 0; i < n; i++ {
		visited := make(map[int]bool)
		root := i
		var leaves []leafDistance
		toTraverse := make([]travelledDistance, 1)
		toTraverse[0] = travelledDistance{root, 0}
		visited[root] = true

		for len(toTraverse) != 0 {
			//pop(0)
			currentTravelled := toTraverse[0]
			currentNode := currentTravelled.node
			distance := currentTravelled.distance
			toTraverse = toTraverse[1:len(toTraverse)]

			added := false

			weightedEdges := node2Edges[currentNode]

			for _, weightedEdge := range weightedEdges {
				weight := weightedEdge.weight
				destination := weightedEdge.destination
				_, ok := visited[destination]
				if !ok {
					visited[destination] = true
					toTraverse = append(toTraverse, travelledDistance{destination, distance + weight})
					added = true
				}
			}
			if added == false {
				leaves = append(leaves, leafDistance{currentNode, distance})
			}
		}

		for _, leafDistance := range leaves {
			leaf := leafDistance.leaf
			distance := leafDistance.distance
			distanceMatrix[i][leaf] = distance
		}
	}

	return distanceMatrix
}

func init() {
	log.SetFlags(log.Lshortfile)
}

func outputToConsole(distanceMatrix [][]int) {
	for i, row := range distanceMatrix {
		for j, _ := range row {
			if j == 0 {
				fmt.Printf("%d", distanceMatrix[i][j])
			} else {
				fmt.Printf("\t%d", distanceMatrix[i][j])
			}
		}
		fmt.Print("\n")
	}
}

func outputToFile(path string, distanceMatrix [][]int) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i, row := range distanceMatrix {
		for j, _ := range row {
			if j == 0 {
				f.WriteString(fmt.Sprintf("%d", distanceMatrix[i][j]))
			} else {
				f.WriteString(fmt.Sprintf("\t%d", distanceMatrix[i][j]))
			}
		}
		f.WriteString(fmt.Sprintf("\n"))
	}
	f.Sync()
}

type solver struct {
}

//exposed
func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	distanceMatrix := solve(input)
	outputToConsole(distanceMatrix)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	distanceMatrix := solve(input)
	outputToFile(outputPath, distanceMatrix)
}

var Solver = solver{}
