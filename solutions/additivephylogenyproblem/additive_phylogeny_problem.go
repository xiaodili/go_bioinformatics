package additivephylogenyproblem

import (
	"bufio"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

//globals
var leaves []tree

//helpers
func check(e error) {
	if e != nil {
		panic(e)
	}
}

//types
type rosalindInput struct {
	n              int
	distanceMatrix [][]int
}

type weightedTree struct {
	weight int
	t      *tree
}

type tree struct {
	value    int
	children map[int]weightedTree
}

//internals
func limb(distanceMatrix [][]int, n int) int {
	j := n - 1
	minimumDistance := math.MaxInt16
	for i := 0; i < n; i++ {
		for k := 0; k < n; k++ {
			if i != j && k != j {
				candidate := (distanceMatrix[i][j] + distanceMatrix[j][k] - distanceMatrix[i][k]) / 2
				if candidate < minimumDistance {
					minimumDistance = candidate
				}
			}
		}
	}
	return minimumDistance
}

func slice(distanceMatrix [][]int, n int) [][]int {
	for i := range distanceMatrix {
		distanceMatrix[i] = distanceMatrix[i][0 : n-1]
	}
	return distanceMatrix[0 : n-1]
}

func getInputFromFile(absfilename string) rosalindInput {

	//opening file
	inputFile, err := os.Open(absfilename)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())

	//distance matrix
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}

	for i := 0; i < n; i++ {
		scanner.Scan()
		line := scanner.Text()
		elements := strings.Split(line, " ")
		for j := 0; j < n; j++ {
			distance, _ := strconv.Atoi(elements[j])
			distanceMatrix[i][j] = distance
		}
	}

	return rosalindInput{n, distanceMatrix}
}

func getInputFromSample() rosalindInput {
	n := 4
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}
	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 21
	distanceMatrix[0][3] = 22

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 12
	distanceMatrix[1][3] = 13

	distanceMatrix[2][0] = 21
	distanceMatrix[2][1] = 12
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 13

	distanceMatrix[3][0] = 22
	distanceMatrix[3][1] = 13
	distanceMatrix[3][2] = 13
	distanceMatrix[3][3] = 0

	return rosalindInput{n, distanceMatrix}
}

func additivePhylogeny(D [][]int, n int) *tree {
	if n == 2 {
		leaves[0] = tree{0, make(map[int]weightedTree)}
		leaves[1] = tree{1, make(map[int]weightedTree)}
		leaves[0].children[1] = weightedTree{D[0][1], &leaves[1]}
		leaves[1].children[0] = weightedTree{D[0][1], &leaves[0]}
		return &leaves[0]
	}
	limbLength := limb(D, n)
	for j := 0; j < n; j++ {
		D[j][n-1] -= limbLength
		D[n-1][j] = D[j][n-1]
	}
	i, k, err := findLeaves(D, n)
	check(err)
	x := D[i][n-1]
	D = slice(D, n)
	T := additivePhylogeny(D, n-1)
	v := assignInternalNode(i, k, x)
	leaves[n-1] = tree{n - 1, make(map[int]weightedTree)}

	//2way attaching
	leaves[n-1].children[v.value] = weightedTree{limbLength, v}
	v.children[n-1] = weightedTree{limbLength, &leaves[n-1]}

	return T
}

func getPath(source, destination *tree) ([]*tree, error) {
	toVisit := [][]*tree{[]*tree{source}}
	for len(toVisit) != 0 {
		//pop
		currentPath := toVisit[0]
		toVisit = toVisit[1:len(toVisit)]

		pathLength := len(currentPath)
		head := currentPath[pathLength-1]
		if head.value == destination.value {
			return currentPath, nil
		}
		for i := range head.children {
			child := head.children[i].t
			if pathLength > 1 {
				previousNode := currentPath[pathLength-2]
				if child.value != previousNode.value {

					newPath := make([]*tree, pathLength+1)
					copy(newPath, append(currentPath, child))

					toVisit = append(toVisit, newPath)
				}
			} else {
				newPath := make([]*tree, pathLength+1)
				copy(newPath, append(currentPath, child))
				toVisit = append(toVisit, newPath)
			}
		}
	}
	return nil, errors.New("can't seem to join source with destination")
}

func assignInternalNode(i int, k int, x int) *tree {
	leafI := leaves[i]
	leafK := leaves[k]
	path, err := getPath(&leafI, &leafK)

	check(err)
	currentPathIndex := 0
	currentNode := &leafI
	for x > 0 {
		currentPathIndex += 1
		nextNode := path[currentPathIndex]
		x -= currentNode.children[nextNode.value].weight

		//update
		currentNode = nextNode
	}

	//yay, found an existing internal node!
	if x == 0 {
		return currentNode
	}

	//otherwise, get the two nodes around internal:
	leftNode := path[currentPathIndex-1]
	rightNode := path[currentPathIndex]
	leftToInternal := leftNode.children[rightNode.value].weight + x
	rightToInternal := -x

	//otherwise, create one:
	nextAvailableIndex := len(leaves)
	newInternal := tree{nextAvailableIndex, make(map[int]weightedTree)}
	leaves = append(leaves, newInternal)

	//also, now need to remove the edges between i and k, and add the new one in:
	delete(leftNode.children, rightNode.value)
	delete(rightNode.children, leftNode.value)

	leftNode.children[nextAvailableIndex] = weightedTree{leftToInternal, &newInternal}
	rightNode.children[nextAvailableIndex] = weightedTree{rightToInternal, &newInternal}
	newInternal.children[leftNode.value] = weightedTree{leftToInternal, leftNode}
	newInternal.children[rightNode.value] = weightedTree{rightToInternal, rightNode}
	return &leaves[nextAvailableIndex]
}

func findLeaves(D [][]int, n int) (int, int, error) {
	for i := 0; i < n; i++ {
		for k := 0; k < n; k++ {
			if D[i][k] == D[i][n-1]+D[n-1][k] {
				return i, k, nil
			}
		}
	}
	return -1, -1, errors.New("couldn't find specified leaves")
}

func exploreTree(leaves []tree) []string {
	var weightedAdjacencyList []string
	for i := range leaves {
		for j := range leaves[i].children {
			source := leaves[i].value
			weight := leaves[i].children[j].weight
			destination := leaves[i].children[j].t.value
			weightedAdjacencyList = append(weightedAdjacencyList, fmt.Sprintf("%d->%d:%d", source, destination, weight))
		}
	}
	return weightedAdjacencyList
}

func solve(rosalindInput rosalindInput) *tree {
	n := rosalindInput.n
	leaves = make([]tree, n)
	return additivePhylogeny(rosalindInput.distanceMatrix, n)
}

func outputToConsole(edges []string) {
	for i := range edges {
		fmt.Println(edges[i])
	}
}

func outputToFile(edges []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range edges {
		f.WriteString(edges[i])
		f.WriteString("\n")
	}
	f.Sync()
}

type solver struct {
}

//exposed
func (s solver) SolveFromSample() {
	input := getInputFromSample()
	_ = solve(input)
	result := exploreTree(leaves)
	outputToConsole(result)
}

func (s solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	_ = solve(input)
	result := exploreTree(leaves)
	outputToFile(result, outputPath)
}

var Solver = solver{}
