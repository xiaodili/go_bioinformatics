package additivephylogenyproblem

import (
	"fmt"
	"testing"
)

//func ExampleSampleSolution() {
//SolveSample()
//// Output:
//// 3
//}

func TestBaseCase(t *testing.T) {
	dimensions := 2
	leaves = make([]tree, dimensions)
	distanceMatrix := make([][]int, dimensions)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, dimensions)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0

	result := additivePhylogeny(distanceMatrix, dimensions)
	actualWeight := result.children[1].weight
	expectedWeight := 13
	if actualWeight != expectedWeight {
		t.Errorf("expected weight: %d, actual weight: %d", expectedWeight, actualWeight)
	}
}

func Test3By3(t *testing.T) {
	dimensions := 3
	leaves = make([]tree, dimensions)
	distanceMatrix := make([][]int, dimensions)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, dimensions)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 21
	distanceMatrix[0][2] = 22

	distanceMatrix[1][0] = 21
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 13

	distanceMatrix[2][0] = 22
	distanceMatrix[2][1] = 13
	distanceMatrix[2][2] = 0

	_ = additivePhylogeny(distanceMatrix, dimensions)
	result := exploreTree(leaves)
	outputToConsole(result)
}

func Test4By4(t *testing.T) {
	dimensions := 4
	leaves = make([]tree, dimensions)
	distanceMatrix := make([][]int, dimensions)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, dimensions)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 21
	distanceMatrix[0][3] = 22

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 12
	distanceMatrix[1][3] = 13

	distanceMatrix[2][0] = 21
	distanceMatrix[2][1] = 12
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 13

	distanceMatrix[3][0] = 22
	distanceMatrix[3][1] = 13
	distanceMatrix[3][2] = 13
	distanceMatrix[3][3] = 0

	//distanceMatrix[0][0] = 0
	//distanceMatrix[0][1] = 21
	//distanceMatrix[0][2] = 22
	//distanceMatrix[0][3] = 13

	//distanceMatrix[1][0] = 21
	//distanceMatrix[1][1] = 0
	//distanceMatrix[1][2] = 13
	//distanceMatrix[1][3] = 12

	//distanceMatrix[2][0] = 22
	//distanceMatrix[2][1] = 13
	//distanceMatrix[2][2] = 0
	//distanceMatrix[2][3] = 13

	//distanceMatrix[3][0] = 13
	//distanceMatrix[3][1] = 12
	//distanceMatrix[3][2] = 13
	//distanceMatrix[3][3] = 0

	_ = additivePhylogeny(distanceMatrix, dimensions)
	result := exploreTree(leaves)
	outputToConsole(result)
}

func TestArraySlice(t *testing.T) {
	n := 3
	distanceMatrix := make([][]int, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]int, n)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 21

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 12

	distanceMatrix[2][0] = 21
	distanceMatrix[2][1] = 12
	distanceMatrix[2][2] = 0

	result := slice(distanceMatrix, n)
	actualRowLength := len(result)
	actualColumnLength := len(result[0])
	expectedRowLength := n - 1
	expectedColumnLength := n - 1
	if actualRowLength != expectedRowLength {
		t.Errorf("expected row length: %d, actual row length: %d", expectedRowLength, actualRowLength)
	}
	if actualColumnLength != expectedColumnLength {
		t.Errorf("expected column length: %d, actual column length: %d", expectedColumnLength, actualColumnLength)
	}
}

func TestExploreTree(t *testing.T) {
	weight := 5
	leaves = make([]tree, 2)
	leaves[0] = tree{0, make(map[int]weightedTree)}
	leaves[1] = tree{1, make(map[int]weightedTree)}
	leaves[0].children[1] = weightedTree{weight, &leaves[1]}
	leaves[1].children[0] = weightedTree{weight, &leaves[0]}
	result := exploreTree(leaves)
	expected1 := fmt.Sprintf("%d->%d:%d", leaves[0].value, leaves[1].value, weight)
	expected2 := fmt.Sprintf("%d->%d:%d", leaves[1].value, leaves[0].value, weight)
	actuallen := len(result)
	if actuallen != 2 {
		t.Errorf("expected length: %d, actual length: %d", 2, actuallen)
	}
	if result[0] != expected1 {
		t.Errorf("expected first result: %s, actual first result: %s", expected1, result[0])
	}
	if result[1] != expected2 {
		t.Errorf("expected second result: %s, actual second result: %s", expected2, result[1])
	}
}
