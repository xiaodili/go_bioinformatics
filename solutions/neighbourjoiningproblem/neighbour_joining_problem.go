package neighbourjoiningproblem

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

//exposed
type solver struct {
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	nodes := solve(input)
	result := exploreTree(nodes)
	outputToConsole(result)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	nodes := solve(input)
	result := exploreTree(nodes)
	outputToFile(result, outputPath)
}

var Solver = solver{}

//types

type rosalindInput struct {
	n              int
	distanceMatrix [][]float32
}

type weightedEdge struct {
	weight float32
	node   *node
}

type node struct {
	id         int
	neighbours map[int]weightedEdge
}

//functions

func solve(rosalindInput rosalindInput) []*node {
	D := rosalindInput.distanceMatrix
	n := rosalindInput.n
	nodes := initialiseNodes(n)
	distanceIndexToNodeIndex := initialiseDi2Ni(n)
	nodes = neighbourJoining(D, n, nodes, distanceIndexToNodeIndex)
	return nodes
}

func initialiseDi2Ni(n int) []int {
	di2ni := make([]int, n, n)
	for i := 0; i < n; i++ {
		di2ni[i] = i
	}
	return di2ni
}

func initialiseNodes(n int) []*node {
	nodes := make([]*node, n, n*2)
	for i := range nodes {
		nodes[i] = &node{id: i, neighbours: make(map[int]weightedEdge)}
	}
	return nodes
}

func neighbourJoining(D [][]float32, n int, nodes []*node, di2ni []int) []*node {

	if n == 2 {
		node1 := di2ni[0]
		node2 := di2ni[1]
		limb1 := D[0][1]
		limb2 := D[0][1]
		nodes[node1].neighbours[node2] = weightedEdge{limb1, nodes[node2]}
		nodes[node2].neighbours[node1] = weightedEdge{limb2, nodes[node1]}
		return nodes
	} else {
		neighbourJoiningMatrix := createNeighbourJoiningMatrix(D)
		i, j := findSmallestElement(neighbourJoiningMatrix)
		delta := (totalDistance(D, i) - totalDistance(D, j)) / float32(n-2)
		limbLength_i := (D[i][j] + delta) / 2
		limbLength_j := (D[i][j] - delta) / 2
		D = createNewLeafFromIJ(D, i, j)
		D = removeRowsAndColumns(D, i, j)

		DCopy := make([][]float32, n-1, n-1)
		for i := range D {
			DCopy[i] = make([]float32, n-1, n-1)
			copy(DCopy[i], D[i])
		}

		nextAvailableNodeIndex := len(nodes)
		newNode := node{id: nextAvailableNodeIndex, neighbours: make(map[int]weightedEdge)}
		nodes = append(nodes, &newNode)

		nodes = neighbourJoining(DCopy, n-1, nodes, updateDistance2NodeIndexMappings(di2ni, i, j, nextAvailableNodeIndex))

		//tree-y stuff here
		nodeI := di2ni[i]
		nodeJ := di2ni[j]
		newNode.neighbours[nodeI] = weightedEdge{node: nodes[nodeI], weight: limbLength_i}
		nodes[nodeI].neighbours[nextAvailableNodeIndex] = weightedEdge{node: &newNode, weight: limbLength_i}
		newNode.neighbours[nodeJ] = weightedEdge{node: nodes[nodeJ], weight: limbLength_j}
		nodes[nodeJ].neighbours[nextAvailableNodeIndex] = weightedEdge{node: &newNode, weight: limbLength_j}
		return nodes
	}
}

func updateDistance2NodeIndexMappings(di2ni []int, c1, c2, newIndex int) []int {
	n := len(di2ni)
	res := make([]int, n, n)
	_ = copy(res, di2ni)
	res = append(res[0:c2], res[c2+1:n]...)
	res = append(res[0:c1], res[c1+1:n-1]...)
	return append(res, newIndex)
}

func removeRowsAndColumns(D [][]float32, i, j int) [][]float32 {
	// slice off the one with the higher index first:
	D = slice(D, j)
	return slice(D, i)
}

func slice(D [][]float32, index int) [][]float32 {
	n := len(D)
	for i := range D {
		D[i] = append(D[i][0:index], D[i][index+1:n]...)
	}
	return append(D[0:index], D[index+1:n]...)
}

func createNewLeafFromIJ(D [][]float32, i, j int) [][]float32 {
	n := len(D)
	mVector := make([]float32, n+1, n+1)
	for k := range D {
		newLeafDistance := (float32(1) / 2) * (D[k][i] + D[k][j] - D[i][j])
		mVector[k] = newLeafDistance
		D[k] = append(D[k], newLeafDistance)
	}
	return append(D, mVector)
}

func createNeighbourJoiningMatrix(D [][]float32) [][]float32 {
	//initialise
	n := len(D)
	neighbourJoiningMatrix := make([][]float32, n)
	for i := range neighbourJoiningMatrix {
		neighbourJoiningMatrix[i] = make([]float32, n)
	}

	//iteration on upper triangle
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			neighbourJoiningMatrix[i][j] = float32((n-2))*D[i][j] - totalDistance(D, i) - totalDistance(D, j)
			neighbourJoiningMatrix[j][i] = neighbourJoiningMatrix[i][j]
		}
	}

	return neighbourJoiningMatrix
}

func findSmallestElement(D [][]float32) (int, int) {
	n := len(D)
	smallestElement := float32(math.MaxFloat32)
	var c1, c2 int
	//iteration on upper triangle
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			if D[i][j] < smallestElement {
				smallestElement = D[i][j]
				c1 = i
				c2 = j
			}
		}
	}
	return c1, c2
}

func totalDistance(D [][]float32, i int) float32 {
	var sum float32
	sum = 0
	for j := range D {
		sum += D[i][j]
	}
	return sum
}

func outputToConsole(edges []string) {
	for i := range edges {
		fmt.Println(edges[i])
	}
}

func outputToFile(edges []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range edges {
		f.WriteString(edges[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func getInputFromSample() rosalindInput {
	n := 4
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 23
	distanceMatrix[0][2] = 27
	distanceMatrix[0][3] = 20

	distanceMatrix[1][0] = 23
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 30
	distanceMatrix[1][3] = 28

	distanceMatrix[2][0] = 27
	distanceMatrix[2][1] = 30
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 30

	distanceMatrix[3][0] = 20
	distanceMatrix[3][1] = 28
	distanceMatrix[3][2] = 30
	distanceMatrix[3][3] = 0

	return rosalindInput{n, distanceMatrix}
}

func getInputFromFile(absfilename string) rosalindInput {

	//opening file
	inputFile, err := os.Open(absfilename)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())

	//distance matrix
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	for i := 0; i < n; i++ {
		scanner.Scan()
		line := scanner.Text()
		elements := strings.Split(line, "\t")
		for j := 0; j < n; j++ {
			distance, _ := strconv.Atoi(elements[j])
			distanceMatrix[i][j] = float32(distance)
		}
	}

	return rosalindInput{n, distanceMatrix}
}

func exploreTree(nodes []*node) []string {
	var weightedAdjacencyList []string
	for i := range nodes {
		for j := range nodes[i].neighbours {
			source := nodes[i].id
			weight := nodes[i].neighbours[j].weight
			destination := nodes[i].neighbours[j].node.id
			weightedAdjacencyList = append(weightedAdjacencyList, fmt.Sprintf("%d->%d:%.3f", source, destination, weight))
		}
	}
	return weightedAdjacencyList
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
