package neighbourjoiningproblem

import (
	"fmt"
	"testing"
)

//func TestNeighbourJoining(t *testing.T) {
//rosalindInput := getInputFromSample()
//D := rosalindInput.distanceMatrix
//_ = D
//n := rosalindInput.n
//nodes := initialiseNodes(n)
//_ = nodes
//neighbourJoining(D, n, nodes)
//node1 := nodes[0]
////have asserted that the nodes are passed by reference
//fmt.Printf("node1: %s\n", node1)
//}

func TestCreateNeighbourJoiningMatrix(t *testing.T) {
	n := 4
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 21
	distanceMatrix[0][3] = 22

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 12
	distanceMatrix[1][3] = 13

	distanceMatrix[2][0] = 21
	distanceMatrix[2][1] = 12
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 13

	distanceMatrix[3][0] = 22
	distanceMatrix[3][1] = 13
	distanceMatrix[3][2] = 13
	distanceMatrix[3][3] = 0

	neighbourJoiningMatrix := createNeighbourJoiningMatrix(distanceMatrix)

	var expected02 float32
	expected02 = -68
	actual02 := neighbourJoiningMatrix[0][1]

	if actual02 != expected02 {
		t.Errorf("expected02: %f, actual02 %f", expected02, actual02)
	}

	var expected31 float32
	expected31 = -60
	actual31 := neighbourJoiningMatrix[3][1]

	if actual31 != expected31 {
		t.Errorf("expected31: %f, actual31 %f", expected31, actual31)
	}
}

func TestQuiz(t *testing.T) {
	n := 4
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 16
	distanceMatrix[0][3] = 10

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 21
	distanceMatrix[1][3] = 15

	distanceMatrix[2][0] = 16
	distanceMatrix[2][1] = 21
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 18

	distanceMatrix[3][0] = 10
	distanceMatrix[3][1] = 15
	distanceMatrix[3][2] = 18
	distanceMatrix[3][3] = 0

	neighbourJoiningMatrix := createNeighbourJoiningMatrix(distanceMatrix)
	for i := range neighbourJoiningMatrix {
		fmt.Printf("%f\n", neighbourJoiningMatrix[i])
	}
}

func TestNeighbourJoining3(t *testing.T) {
	n := 3
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 13
	distanceMatrix[0][2] = 10

	distanceMatrix[1][0] = 13
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 11

	distanceMatrix[2][0] = 10
	distanceMatrix[2][1] = 11
	distanceMatrix[2][2] = 0

	rosalindInput := rosalindInput{n: n, distanceMatrix: distanceMatrix}
	nodes := solve(rosalindInput)
	actualNumKeys := len(nodes[0].neighbours)
	expectedNumKeys := 2
	if actualNumKeys != expectedNumKeys {
		t.Errorf("actual: %d, expected: %d", actualNumKeys, expectedNumKeys)
	}
}
