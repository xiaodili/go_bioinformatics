package outcomelikelihoodproblem

import (
	"fmt"
	"strconv"
	"testing"
)

func TestSolveFromSample(t *testing.T) {
	input := getInputFromSample()
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 15, 64)
	fmt.Println(probabilityStr)
}
