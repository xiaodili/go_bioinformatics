package outcomelikelihoodproblem

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type solver struct {
}

var Solver = solver{}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 15, 64)
	outputToFile([]string{probabilityStr}, outputPath)
}

func outputToFile(formattedOutput []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range formattedOutput {
		f.WriteString(formattedOutput[i])
		f.WriteString("\n")
	}
	f.Sync()
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	probability := solve(input)
	probabilityStr := strconv.FormatFloat(probability, 'e', 15, 64)
	fmt.Println(probabilityStr)
}

func solve(ri rosalindInput) float64 {
	//initialisation:
	emittedString := ri.emittedString
	alphabet := ri.alphabet
	letterToIndex := make(map[byte]int)
	for index, value := range alphabet {
		letterToIndex[value] = index
	}
	states := ri.states
	numStates := len(states)
	stateToIndex := make(map[byte]int)
	for index, value := range states {
		stateToIndex[value] = index
	}
	transition := ri.transition
	emission := ri.emission

	forwardMatrix := make([][]float64, len(emittedString), len(emittedString))
	for i, _ := range forwardMatrix {
		forwardMatrix[i] = make([]float64, numStates, numStates)
	}
	for j, _ := range forwardMatrix[0] {
		forwardMatrix[0][j] = 0.5 * float64(emission[j][letterToIndex[emittedString[0]]])
	}

	for i := 1; i < len(emittedString); i++ {
		for j, _ := range forwardMatrix[i] {
			transEmissions := make([]float64, numStates, numStates)
			for k, prevProb := range forwardMatrix[i-1] {
				transEmissions[k] = prevProb * float64(transition[k][j]) *
					float64(emission[j][letterToIndex[emittedString[i]]])
			}
			forwardMatrix[i][j] = sumFloat64(transEmissions)
		}
	}

	totalProbability := sumFloat64(forwardMatrix[len(emittedString)-1])
	return totalProbability
}

func getInputFromSample() rosalindInput {
	emittedString := "xzyyzzyzyy"
	alphabet := []byte{'x', 'y', 'z'}
	states := []byte{'A', 'B'}
	transition := make([][]float32, 2)
	transition[0] = []float32{0.303, 0.697}
	transition[1] = []float32{0.831, 0.169}
	emission := make([][]float32, 2)
	emission[0] = []float32{0.533, 0.065, 0.402}
	emission[1] = []float32{0.342, 0.334, 0.324}
	return rosalindInput{emittedString, alphabet, states, transition, emission}
}

func getInputFromFile(absPath string) rosalindInput {
	//opening file
	inputFile, err := os.Open(absPath)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	emittedString := scanner.Text()

	scanner.Scan() //-------

	scanner.Scan()
	alphabetstr := scanner.Text()
	alphabetstrs := strings.Split(alphabetstr, "\t")
	alphabet := make([]byte, len(alphabetstrs), len(alphabetstrs))
	for i, value := range alphabetstrs {
		alphabet[i] = value[0]
	}

	scanner.Scan() //-------

	scanner.Scan()
	statesStr := scanner.Text()
	stateStrs := strings.Split(statesStr, "\t")
	states := make([]byte, len(stateStrs), len(stateStrs))
	for i, value := range stateStrs {
		states[i] = value[0]
	}

	scanner.Scan() //-------
	scanner.Scan() // A B

	transitions := make([][]float32, len(states), len(states))
	for rowNum := 0; rowNum < len(states); rowNum++ {
		scanner.Scan()
		transitionRowStr := strings.Split(scanner.Text(), "\t")[1:]
		transitions[rowNum] = make([]float32, len(states))
		for i := 0; i < len(states); i++ {
			transitionElem, _ := strconv.ParseFloat(transitionRowStr[i], 32)
			transitions[rowNum][i] = float32(transitionElem)
		}
	}

	scanner.Scan() //-------
	scanner.Scan() // x y z

	emissions := make([][]float32, len(states), len(states))
	for rowNum := 0; rowNum < len(states); rowNum++ {
		scanner.Scan()
		emissionRowStr := strings.Split(scanner.Text(), "\t")[1:]
		emissions[rowNum] = make([]float32, len(alphabet))
		for i := 0; i < len(alphabet); i++ {
			emissionElem, _ := strconv.ParseFloat(emissionRowStr[i], 32)
			emissions[rowNum][i] = float32(emissionElem)
		}
	}

	return rosalindInput{emittedString, alphabet, states, transitions, emissions}
}

type rosalindInput struct {
	emittedString string
	alphabet      []byte
	states        []byte
	transition    [][]float32
	emission      [][]float32
}

func sumFloat64(floats []float64) float64 {
	total := float64(0)
	for _, value := range floats {
		total += value
	}
	return total
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
