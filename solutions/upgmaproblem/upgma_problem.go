package upgmaproblem

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

//exposed
type solver struct {
}

func (_ solver) SolveFromSample() {
	input := getInputFromSample()
	nodes := solve(input)
	result := exploreTree(nodes)
	outputToConsole(result)
}

func (_ solver) Solve(inputPath, outputPath string) {
	input := getInputFromFile(inputPath)
	nodes := solve(input)
	result := exploreTree(nodes)
	outputToFile(result, outputPath)
}

var Solver = solver{}

//helpers
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func outputToFile(edges []string, path string) {
	f, err := os.Create(path)
	check(err)
	defer f.Close()
	for i := range edges {
		f.WriteString(edges[i])
		f.WriteString("\n")
	}
	f.Sync()
}

//types
type weightedNode struct {
	length float32
	node   *node
}

type node struct {
	id     int
	age    float32
	parent weightedNode
	left   weightedNode
	right  weightedNode
}

func (n *node) setParent(w weightedNode) {
	n.parent = w
}
func (n *node) setLeft(w weightedNode) {
	n.left = w
}
func (n *node) setRight(w weightedNode) {
	n.right = w
}

func (n *node) numLeaves() int {
	leaves := 0
	toTraverse := []*node{n}
	for len(toTraverse) != 0 {
		currentNode := toTraverse[0]
		toTraverse = toTraverse[1:len(toTraverse)]
		if currentNode.left.node != nil {
			toTraverse = append(toTraverse, currentNode.left.node)
		}
		if currentNode.right.node != nil {
			toTraverse = append(toTraverse, currentNode.right.node)
		}
		if currentNode.right.node == nil && currentNode.left.node == nil {
			leaves += 1
		}
	}
	return leaves
}

type rosalindInput struct {
	n              int
	distanceMatrix [][]float32
}

//functions
func getInputFromFile(absfilename string) rosalindInput {

	//opening file
	inputFile, err := os.Open(absfilename)
	check(err)
	defer inputFile.Close()
	scanner := bufio.NewScanner(inputFile)

	//parsing
	scanner.Scan()
	n, _ := strconv.Atoi(scanner.Text())

	//distance matrix
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	for i := 0; i < n; i++ {
		scanner.Scan()
		line := scanner.Text()
		elements := strings.Split(line, "\t")
		for j := 0; j < n; j++ {
			distance, _ := strconv.Atoi(elements[j])
			distanceMatrix[i][j] = float32(distance)
		}
	}

	return rosalindInput{n, distanceMatrix}
}

func getInputFromSample() rosalindInput {
	n := 4
	distanceMatrix := make([][]float32, n)
	for i := range distanceMatrix {
		distanceMatrix[i] = make([]float32, n)
	}

	distanceMatrix[0][0] = 0
	distanceMatrix[0][1] = 20
	distanceMatrix[0][2] = 17
	distanceMatrix[0][3] = 11

	distanceMatrix[1][0] = 20
	distanceMatrix[1][1] = 0
	distanceMatrix[1][2] = 20
	distanceMatrix[1][3] = 13

	distanceMatrix[2][0] = 17
	distanceMatrix[2][1] = 20
	distanceMatrix[2][2] = 0
	distanceMatrix[2][3] = 10

	distanceMatrix[3][0] = 11
	distanceMatrix[3][1] = 13
	distanceMatrix[3][2] = 10
	distanceMatrix[3][3] = 0

	return rosalindInput{n, distanceMatrix}
}

func exploreTree(nodes []*node) []string {
	var weightedAdjacencyList []string
	for i := range nodes {
		sourceId := nodes[i].id

		if nodes[i].left.node != nil {
			leftChildId := nodes[i].left.node.id
			leftEdgeLength := nodes[i].left.length
			weightedAdjacencyList = append(weightedAdjacencyList, fmt.Sprintf("%d->%d:%.3f", sourceId, leftChildId, leftEdgeLength))
		}

		if nodes[i].right.node != nil {
			rightChildId := nodes[i].right.node.id
			rightEdgeLength := nodes[i].right.length
			weightedAdjacencyList = append(weightedAdjacencyList, fmt.Sprintf("%d->%d:%.3f", sourceId, rightChildId, rightEdgeLength))
		}

		if nodes[i].parent.node != nil {
			parentChildId := nodes[i].parent.node.id
			parentEdgeLength := nodes[i].parent.length
			weightedAdjacencyList = append(weightedAdjacencyList, fmt.Sprintf("%d->%d:%.3f", sourceId, parentChildId, parentEdgeLength))
		}
	}
	return weightedAdjacencyList
}

func outputToConsole(edges []string) {
	for i := range edges {
		fmt.Println(edges[i])
	}
}

func findClosestClusters(D [][]float32) (int, int) {
	n := len(D)
	minimumDistance := float32(math.MaxFloat32)
	var c1, c2 int
	//iteration on upper triangle
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			if D[i][j] < minimumDistance {
				minimumDistance = D[i][j]
				c1 = i
				c2 = j
			}
		}
	}
	return c1, c2
}

func removeRowsAndColumns(D [][]float32, i, j int) [][]float32 {
	// slice off the one with the higher index first:
	D = slice(D, j)
	return slice(D, i)
}

func slice(D [][]float32, index int) [][]float32 {
	n := len(D)
	for i := range D {
		D[i] = append(D[i][0:index], D[i][index+1:n]...)
	}
	return append(D[0:index], D[index+1:n]...)
}

func calculateNewDistances(D [][]float32, c1, c2, c1leaves, c2leaves int) []float32 {
	n := len(D)
	newDistances := make([]float32, n-1, n-1)
	currentIndex := 0
	for i := range D {
		if i != c1 && i != c2 {
			newDistances[currentIndex] = (D[i][c1]*float32(c1leaves) + D[i][c2]*float32(c2leaves)) / float32(c1leaves+c2leaves)
			currentIndex += 1
		}
	}
	newDistances[currentIndex] = 0
	return newDistances
}

func appendNewDistances(D [][]float32, newDistances []float32) [][]float32 {
	for i := range D {
		D[i] = append(D[i], newDistances[i])
	}
	return append(D, newDistances)
}

func updateDistance2NodeIndexMappings(di2ni []int, c1, c2, newIndex int) []int {
	di2ni = append(di2ni[0:c2], di2ni[c2+1:len(di2ni)]...)
	di2ni = append(di2ni[0:c1], di2ni[c1+1:len(di2ni)]...)
	return append(di2ni, newIndex)
}

func solve(rosalindInput rosalindInput) []*node {
	//initialisation
	D := rosalindInput.distanceMatrix
	n := rosalindInput.n
	distanceIndexToNodeIndex := make([]int, n, n)
	for i := 0; i < n; i++ {
		distanceIndexToNodeIndex[i] = i
	}
	nodes := make([]*node, n, n*2)
	for i := 0; i < n; i++ {
		nodes[i] = &node{id: i, age: 0}
	}

	//creating the tree
	for len(D) != 1 {
		c1, c2 := findClosestClusters(D)
		age := D[c1][c2] / 2
		nextAvailableNodeIndex := len(nodes)
		newNode := node{id: nextAvailableNodeIndex, age: age}
		nodes = append(nodes, &newNode)

		leftNode := nodes[distanceIndexToNodeIndex[c1]]
		rightNode := nodes[distanceIndexToNodeIndex[c2]]

		newNode.setLeft(weightedNode{node: leftNode, length: age - leftNode.age})
		newNode.setRight(weightedNode{node: rightNode, length: age - rightNode.age})
		leftNode.setParent(weightedNode{node: &newNode, length: age - leftNode.age})
		rightNode.setParent(weightedNode{node: &newNode, length: age - rightNode.age})

		newDistances := calculateNewDistances(D, c1, c2, leftNode.numLeaves(), rightNode.numLeaves())
		D = removeRowsAndColumns(D, c1, c2)
		D = appendNewDistances(D, newDistances)
		distanceIndexToNodeIndex = updateDistance2NodeIndexMappings(distanceIndexToNodeIndex, c1, c2, nextAvailableNodeIndex)
	}

	return nodes
}
