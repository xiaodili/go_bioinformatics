package upgmaproblem

import (
	"testing"
)

func TestFindClosestClusters(t *testing.T) {
	input := getInputFromSample()
	D := input.distanceMatrix
	c1, c2 := findClosestClusters(D)
	expectedC1 := 2
	expectedC2 := 3
	if c1 != expectedC1 {
		t.Errorf("expected c1: %d, actual c1: %d", expectedC1, c1)
	}
	if c2 != expectedC2 {
		t.Errorf("expected c2: %d, actual c2: %d", expectedC2, c2)
	}
}

func TestRemoveRowsAndColumns(t *testing.T) {
	input := getInputFromSample()
	D := input.distanceMatrix
	D = removeRowsAndColumns(D, 0, 1)
	actualLen := len(D)
	expectedLen := 2
	if actualLen != expectedLen {
		t.Errorf("expected len: %d, actual len: %d", expectedLen, actualLen)
	}
	var expected00 float32
	expected00 = 0
	if D[0][0] != expected00 {
		t.Errorf("expected [0][0]: %f, actual [0][0]: %f", expected00, D[0][0])
	}

	var expected01 float32
	expected01 = 10
	if D[0][1] != expected01 {
		t.Errorf("expected [0][1]: %f, actual [0][1]: %f", expected01, D[0][0])
	}
}

func TestCalculateNewDistances(t *testing.T) {
	input := getInputFromSample()
	D := input.distanceMatrix
	newDistances := calculateNewDistances(D, 2, 3, 1, 1)
	var expectedD1 float32
	expectedD1 = 14
	if newDistances[0] != expectedD1 {
		t.Errorf("expected expectedD1: %f, actual D1: %f", expectedD1, newDistances[0])
	}

	var expectedD2 float32
	expectedD2 = 16.5
	if newDistances[1] != expectedD2 {
		t.Errorf("expected expectedD2: %f, actual D2: %f", expectedD2, newDistances[1])
	}

	expectedLen := len(D) - 1
	actualLen := len(newDistances)
	if expectedLen != actualLen {
		t.Errorf("expectedLen: %f, actualLen: %f", expectedLen, actualLen)
	}
}

func TestUpdateIndexMappings1(t *testing.T) {
	n := 4
	distanceIndexToNodeIndex := make([]int, n, n)
	for i := 0; i < n; i++ {
		distanceIndexToNodeIndex[i] = i
	}

	di2ni := updateDistance2NodeIndexMappings(distanceIndexToNodeIndex, 2, 3, 4)
	expectedDi2ni := []int{0, 1, 4}
	expectedLen := n - 1
	actualLen := len(di2ni)
	if expectedLen != actualLen {
		t.Errorf("expected len: %d, actual len: %d", expectedLen, actualLen)
	}
	for i := range di2ni {
		if di2ni[i] != expectedDi2ni[i] {
			t.Errorf("expected element: %d, actual element: %d", expectedDi2ni[i], di2ni[i])
		}
	}
}
