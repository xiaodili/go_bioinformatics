package main

import (
	"bioinformatics/solutions/additivephylogenyproblem"
	"bioinformatics/solutions/decodingproblem"
	"bioinformatics/solutions/distancebetweenleavesproblem"
	"bioinformatics/solutions/hierarchicalclusteringproblem"
	"bioinformatics/solutions/largeparsimonyproblem"
	"bioinformatics/solutions/limblengthproblem"
	"bioinformatics/solutions/nearestneighboursproblem"
	"bioinformatics/solutions/neighbourjoiningproblem"
	"bioinformatics/solutions/outcomelikelihoodproblem"
	"bioinformatics/solutions/probabilityofahiddenpathproblem"
	"bioinformatics/solutions/probabilityofanoutcomegivenahiddenpathproblem"
	"bioinformatics/solutions/smallparsimonyproblem"
	"bioinformatics/solutions/smallparsimonyunrootedproblem"
	"bioinformatics/solutions/upgmaproblem"
	"bioinformatics/util"
	"os"
	"path/filepath"
)

func main() {
	problem_driver := make(map[string]util.Solver)
	problem_driver["additive_phylogeny"] = additivephylogenyproblem.Solver
	problem_driver["limb_length"] = limblengthproblem.Solver
	problem_driver["distance_between_leaves"] = distancebetweenleavesproblem.Solver
	problem_driver["upgma"] = upgmaproblem.Solver
	problem_driver["neighbour_joining"] = neighbourjoiningproblem.Solver
	problem_driver["small_parsimony"] = smallparsimonyproblem.Solver
	problem_driver["small_parsimony_unrooted"] = smallparsimonyunrootedproblem.Solver
	problem_driver["nearest_neighbours"] = nearestneighboursproblem.Solver
	problem_driver["large_parsimony"] = largeparsimonyproblem.Solver
	problem_driver["hierarchical_clustering"] = hierarchicalclusteringproblem.Solver
	problem_driver["probability_of_a_hidden_path"] = probabilityofahiddenpathproblem.Solver
	problem_driver["probability_of_an_outcome_given_a_hidden_path"] = probabilityofanoutcomegivenahiddenpathproblem.Solver
	problem_driver["decoding"] = decodingproblem.Solver
	problem_driver["outcome_likelihood"] = outcomelikelihoodproblem.Solver

	numArgs := len(os.Args)
	if numArgs == 2 {
		problem := os.Args[1]
		solver, ok := problem_driver[problem]
		if ok {
			solver.SolveFromSample()
		} else {
			panic("invalid problem specified!")
		}
	} else if numArgs == 4 {
		problem := os.Args[1]
		solver, ok := problem_driver[problem]
		if ok {
			inputPath, _ := filepath.Abs("assets/" + os.Args[2])
			outputPath, _ := filepath.Abs("assets/" + os.Args[3])
			solver.Solve(inputPath, outputPath)
		} else {
			panic("invalid problem specified!")
		}
	} else {
		panic("incorrect number of args!")
	}
}
