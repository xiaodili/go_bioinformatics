package util

//If you want to implement these methods in a different package, they need to be public
type Solver interface {
	SolveFromSample()
	Solve(inputPath, outputPath string)
}
